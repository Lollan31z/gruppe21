package org.prog3.musicplayer2021.Database;

import javax.persistence.*;
import org.prog3.musicplayer2021.Functions.*;
import org.prog3.musicplayer2021.Exceptions.*;
import java.util.*;

/**
 * Database
 * 
 * methods for communicating with the database
 * 
 * @author brian mueller & pascal vinzent
 *
 */
public class Database {
	private static final String ENTITY_MANAGER_LINK = "org.prog3.musicplayer2021";
	private static EntityManagerFactory ENTITY_MANAGER_FACTORY = Persistence
			       .createEntityManagerFactory(ENTITY_MANAGER_LINK);
	
	
	/**
	 * addSong
	 * 
	 * adds a Song to the database
	 * 
	 * @param song (will be added to database)
	 * @throws Exception 
	 */
	public static void addSong(Song song){
		EntityManager em = ENTITY_MANAGER_FACTORY.createEntityManager();
		EntityTransaction et = null;
		try {
			et = em.getTransaction();
			et.begin();
			em.persist(song);
			et.commit();
		}
		catch(Exception e) {
		if(et != null) {
			et.rollback();
		}
		}
		finally {
			em.close();
		      }
		}
	
	
	/**
	 * addLatestSongToPlaylist
	 * 
	 * adds the latest song to the "AllSongs"-playlist
	 * 
	 * @throws Exception
	 */
	public static void addLatestSongToPlaylist() throws MP3PlayerException {
		List<Song> all = Database.findAllSongs();
		Playlist allSongs = Database.findPlaylistByName("AllSongs");
		Database.addSongToPlaylist(all.get(all.size()-1).getSongId(), allSongs.getPlaylistId());
	}
	
	
	/**
	 * findSongByPrimaryKey
	 * 
	 * finds a Song in the database and returns it
	 * 
	 * @param songId (should be an actual song-ID)
	 * @return song (returns a song, if no song is found -> returns null)
	 */
	public static Song findSongByPrimaryKey(final int songId){
		EntityManager em = ENTITY_MANAGER_FACTORY.createEntityManager();
		EntityTransaction et = null;
		try {
		et = em.getTransaction();
		et.begin();
		final Song song = (Song)em.find(Song.class, songId);	
		et.commit();
		return song;
		}
		catch(Exception e) {
		    if(e != null) {
			et.rollback();
		}
		    throw e;
		}
		    finally {
		    	em.close();
		    }
	}
	
	
	/**
	 * findSongByName
	 * 
	 * finds a song in the database with the name given in "songName"
	 * 
	 * @param title (name that needs to be found)
	 * @return Song (gives back the song, if there is no song -> returns null)
	 * 
	 */
	public static List<Song> findSongByTitle(final String title){
		EntityManager em = ENTITY_MANAGER_FACTORY.createEntityManager();
		EntityTransaction et = null;
		final List<Song> resultList;
		final String queryString = "SELECT s FROM Song s WHERE s.title = " + "'"+ title + "'";
		try {
		et = em.getTransaction();
		et.begin();
		Query query = em.createQuery(queryString);
		resultList = (List <Song>) query.getResultList();	
		et.commit();
		return resultList;
		}
		catch(Exception e) {
		    if(e != null) {
			et.rollback();
		}
		    throw e;
		}
		    finally {
		    	em.close();
		    }
	}
	
	
	/**
	 * findSongByPath
	 * 
	 * find a Song by searching the path in the database and giving it back
	 * 
	 * @param path
	 * @return song
	 */
	public static Song findSongByPath(final String path) {
		EntityManager em = ENTITY_MANAGER_FACTORY.createEntityManager();
		EntityTransaction et = null;
		final String queryString = "SELECT s FROM Song s WHERE s.path = " + "'"+ path + "'";
		try {
			et = em.getTransaction();
			et.begin();
			Query query = em.createQuery(queryString);
			final Song song = (Song)query.getSingleResult();	
			et.commit();
			return song;
		}
		catch(Exception e) {
		    if(e != null) {
			et.rollback();
		}
		    throw e;
		}
		    finally {
		    	em.close();
		    }
	}
	
	
	/**
	 * find SongByArtist
	 * 
	 * finds Songs in the database that are made by the Artist
	 * 
	 * @param artistName (the Name of the Artist)
	 * @return resultList (returns a list of songs if no songs are found -> returns null)
	 *
	 */
	public static List<Song> findSongsByArtist(final String artistName) {
		EntityManager em = ENTITY_MANAGER_FACTORY.createEntityManager();
		EntityTransaction et = null;
		final List<Song> resultList;
		final String queryString = "SELECT s FROM Song s WHERE s.artist = " + "'"+ artistName + "'";
		try {
		et = em.getTransaction();
		et.begin();
		Query query = em.createQuery(queryString);
		resultList = (List <Song>) query.getResultList();	
		et.commit();
		return resultList;
		}
		catch(Exception e) {
		    if(e != null) {
			et.rollback();
		}
		    throw e;
		}
		    finally {
		    	em.close();
		    }
		
	}
	
	
	/**
	 * findSongsByAlbumName
	 * 
	 * finds songs in the database by the given "albumname" and gives them in a list back
	 * 
	 * @param albumName (the name of the album that was published with the song)
	 * @return resultList (a list of songs if no songs are found -> returns null)
	 * 
	 */
	public static List<Song> findSongsByAlbumName(final String albumName) {
		EntityManager em = ENTITY_MANAGER_FACTORY.createEntityManager();
		EntityTransaction et = null;
	    final List<Song> resultList;
		final String queryString = "SELECT s FROM Song s WHERE s.album = " + "'"+ albumName + "'";
		try {
		et = em.getTransaction();
		et.begin();
		Query query = em.createQuery(queryString);
		resultList = (List<Song>)  query.getResultList();	
		et.commit();
		return resultList;
		}
		catch(Exception e) {
		    if(e != null) {
			et.rollback();
		}
		    throw e;
		}
		    finally {
		    	em.close();
		    }
		
	}
	
	
	/**
	 * findAllSongs
	 * 
	 * finds all songs in the database and gives them back in a list
	 * 
	 * @return resultList (list of songs)
	 */
	public static List<Song> findAllSongs(){
		EntityManager em = ENTITY_MANAGER_FACTORY.createEntityManager();
		EntityTransaction et = null;
	    final List<Song> resultList;
		final String queryString = "SELECT s FROM Song s";
		try {
		et = em.getTransaction();
		et.begin();
		Query query = em.createQuery(queryString);
		resultList = (List<Song>)  query.getResultList();	
		et.commit();
		return resultList;
		}
		catch(Exception e) {
		    if(e != null) {
			et.rollback();
		}
		    throw e;
		}
		    finally {
		    	em.close();
		    }
		
	}
	
	
	/**
	 * findAllSongsAsPlaylist
	 * 
	 * finds all songs in database and gives theses songs as playlist back
	 * 
	 * @return result (playlist with all songs)
	 * @throws Exception, PlaylistException 
	 */
	public static Playlist findAllSongsAsPlaylist () throws MP3PlayerException{
		EntityManager em = ENTITY_MANAGER_FACTORY.createEntityManager();
		EntityTransaction et = null;
		final Playlist result = new Playlist("AllSongs");
	    final List<Song> resultList;
		final String queryString = "SELECT s FROM Song s";
		try {
		et = em.getTransaction();
		et.begin();
		Query query = em.createQuery(queryString);
		resultList = (List<Song>)  query.getResultList();	
		et.commit();
		for(Song s :resultList) {
			result.addSongToPlaylist(s);
		}
		return result;
		}
		catch(MP3PlayerException e) {
			    if(e != null) {
				et.rollback();
			}
			    throw e;
		}
		catch(Exception e) {
		    if(e != null) {
			et.rollback();
		}
		    throw e;
		}
		    finally {
		    	em.close();
		    }
		
	}
	
	
	/**
	 * deleteSong
	 * 
	 * deletes a song from the database
	 * 
	 * @param songId (songId from the song that should be deleted)
	 *
	 */
	public static void deleteSong(final int songId) {
		EntityManager em = ENTITY_MANAGER_FACTORY.createEntityManager();
		EntityTransaction et = null;
		try {
			et = em.getTransaction();
			et.begin();
			final Song songToBeDeleted = (Song)em.find(Song.class, songId);
			if (songToBeDeleted != null) {
				em.remove(songToBeDeleted);
			}

			et.commit();		
			}
		catch(NoResultException e) {
			System.out.println("e");
			if(et != null) {
				et.rollback();
			}
		}
		finally {
			em.close();
		}
	}
	
	
	/** isInDataBaseSong
	 * 
	 * looks up if a song by the chosen name and column exists in the database
	 * 
	 * @param name (name that you are searching for)
	 * @param column (column where you are searching the name for)
	 * @return exists true
	 * @return not exists false
	 */
	public static boolean isInDatabaseSong(String name, String column) {
		EntityManager em = ENTITY_MANAGER_FACTORY.createEntityManager();
		EntityTransaction et = null;
		boolean angabe = true;
	    final List<Song> resultList;
		final String queryString = "SELECT s FROM Song s WHERE s."+ column +"= " + "'"+ name + "'";
		try {
			et = em.getTransaction();
			et.begin();
			Query query = em.createQuery(queryString);
			resultList = (List<Song>)  query.getResultList();
			if(resultList.size() == 0) {
				angabe = false;
			}
			et.commit();
			}
			catch(NoResultException e) {
				if(e != null) {
					et.rollback();
				}
		}
		catch(Exception e) {
		    if(e != null) {
			et.rollback();
		}
		}
		    finally {
		    	em.close();
		    	return angabe;
		    }
		

	}
	
	
	/**
	 * addPlaylist
	 * 
	 * adds a playlist to the database
	 * 
	 * @param playlistName (playlist name)
	 * @param date (date where the playlist was created)
	 * 
	 */
	public static void addPlaylist(Playlist playlist) {
		EntityManager em = ENTITY_MANAGER_FACTORY.createEntityManager();
		EntityTransaction et = null;
		try {
			et = em.getTransaction();
			et.begin();
			em.persist(playlist);
			et.commit();
		}
		catch(NoResultException e) {
			if(et != null) {
				et.rollback();
			}
		}
		finally {
				em.close();
			}
		}
	
	
	/**
	 * findPlaylistByPrimaryKey
	 * 
	 * finds a playlist by the given playlistId in the database and gives it back
	 * 
	 * @param playlistId (primary key - playlistId)
	 * @return a playlist (when no result is found -> returns null)
	 *
	 */
	public static Playlist findPlaylistByPrimaryKey(final int playlistId) {
		EntityManager em = ENTITY_MANAGER_FACTORY.createEntityManager();
		EntityTransaction et = null;
		
		try {
			et = em.getTransaction();
			et.begin();
			final Playlist result = (Playlist)em.find(Playlist.class, playlistId);
			et.commit();
			return result;
		}
		catch(NoResultException e) {
			if(et != null) {
				et.rollback();
		}
		    throw e;
	}
		finally {
			em.close();
		}
	}
	
	
	/**
	 * findPlaylistByName
	 * 
	 * finds a playlist by the given name in the database and gives it back
	 * 
	 * @param playlistname (primary key - playlist name)
	 * @return a playlist (when no result is found -> returns null)
	 *
	 */
	public static Playlist findPlaylistByName(final String playlistname) {
		EntityManager em = ENTITY_MANAGER_FACTORY.createEntityManager();
		EntityTransaction et = null;
		final Playlist result;
		final String queryString = "SELECT p FROM Playlist p WHERE p.playlistName = " + "'"+ playlistname + "'";
		try {
			et = em.getTransaction();
			et.begin();
			Query query = em.createQuery(queryString);
			result = (Playlist) query.getSingleResult();
			et.commit();
			return result;
		}
		catch(Exception e) {
			if(et != null) {
				et.rollback();
				return null;
		}
		    throw e;
	}
		finally {
			em.close();
		}
	}
	
	
	/**
	 * isInDatabaseByNamePlaylist
	 * 
	 * looks up if a playlist exists by the name
	 * 
	 * @param name
	 * @return exists true
	 * @return not exists false
	 */
	public static boolean isInDatabaseByNamePlaylist(String name) {
		EntityManager em = ENTITY_MANAGER_FACTORY.createEntityManager();
		EntityTransaction et = null;
		final Playlist result;
		final String queryString = "SELECT p FROM Playlist p WHERE p.playlistName =" +  "'"+ name + "'";
		try {
			et = em.getTransaction();
			et.begin();
			Query query = em.createQuery(queryString);
			result = (Playlist) query.getSingleResult();
			et.commit();

		
		
		}
		catch(Exception e) {
			if(et != null) {
				et.rollback();
				return false;
		}
	}
		finally {
			em.close();
		}
		return true;
	}
	
	
	/**
	 * addSongToPlaylist
	 * 
	 * adds songid and playlistname to the table "isin" 
	 * 
	 * @param songId (songId of the song that's to be added)
	 * @param playlistname (playlistname that the song should be added to)
	 * @throws Exception 
	 */
	public static void addSongToPlaylist(final int songId, final int playlistId) throws MP3PlayerException {
		EntityManager em = ENTITY_MANAGER_FACTORY.createEntityManager();
		EntityTransaction et = null;
	    final Playlist playlist;
		final Song song;
		try {
			et = em.getTransaction();
			et.begin();
			playlist = (Playlist)em.find(Playlist.class, playlistId);
			song = (Song)em.find(Song.class, songId);
			playlist.addSongToPlaylist(song);
			em.persist(playlist);
			et.commit();
		}
		catch(MP3PlayerException e) {
			if(et != null) {
				et.rollback();
				throw e;
		}
	}
		finally {
			em.close();
		}
	}
	

	/**
	 * showSongsInPlaylist
	 * 
	 * shows all songs in the playlist
	 * 
	 * @param playlistId
	 * @return resultlist (all songs in the playlist)
	 */
	public static List<Song> showSongsInPlaylist (int playlistId) {
		EntityManager em = ENTITY_MANAGER_FACTORY.createEntityManager();
		EntityTransaction et = null;
	    final Playlist result;
	    final List<Song> resultList;
		final String queryString = "SELECT p FROM Playlist p WHERE p.playlistId = " + playlistId;
		try {
		et = em.getTransaction();
		et.begin();
		Query query = em.createQuery(queryString);
		result = (Playlist) query.getSingleResult();
		resultList = (List<Song>)result.getSonglist();
		et.commit();
		return resultList;
		}
		catch(Exception e) {
		    if(e != null) {
			et.rollback();
		}
		    throw e;
		}
		    finally {
		    	em.close();
		    }
		
	}
	
	
    /**
     * deletePlaylist
     * 
     * deletes a playlist from the database
     * 
     * @param playlistId
     */
	public static void deletePlaylist(final int playlistId) {
		EntityManager em = ENTITY_MANAGER_FACTORY.createEntityManager();
		EntityTransaction et = null;
		try {
			et = em.getTransaction();
			et.begin();
			final Playlist playlistToBeDeleted = (Playlist)em.find(Playlist.class, playlistId);
			if (playlistToBeDeleted != null) {
				playlistToBeDeleted.getSonglist().clear();
				em.remove(playlistToBeDeleted);
			}

			et.commit();		
			}
		catch(NoResultException e) {
			if(et != null) {
				et.rollback();
			}
		    throw e;
		}
		finally {
			em.close();
		}
	}
	
	
	/**
	 * deleteSongFromPlaylist
	 * 
	 * deletes a song from a playlist and the association in the database
	 * 
	 * @param playlistId
	 * @param songId
	 */
	public static void deleteSongFromPlaylist(final int playlistId, final int songId) {
		EntityManager em = ENTITY_MANAGER_FACTORY.createEntityManager();
		EntityTransaction et = null;
		try {
			et = em.getTransaction();
			et.begin();
			final Playlist playlistToBeDeleted = (Playlist)em.find(Playlist.class, playlistId);
			final Song songToBeDeleted =(Song)em.find(Song.class, songId);
			if (playlistToBeDeleted != null) {
				playlistToBeDeleted.getSonglist().remove(songToBeDeleted);
				em.persist(playlistToBeDeleted);
			}

			et.commit();		
			}
		catch(NoResultException e) {
			if(et != null) {
				et.rollback();
			}
		    throw e;
		}
		finally {
			em.close();
		}
	}
	
	
	/**
	 * showAllPlaylists
	 * 
	 * returns a list including all playlists in the database
	 * 
	 */
	public static List<Playlist> showAllPlaylists () {
		EntityManager em = ENTITY_MANAGER_FACTORY.createEntityManager();
		EntityTransaction et = null;
	    final List<Playlist> resultList;
		final String queryString = "SELECT p FROM Playlist p"; 
		try {
		et = em.getTransaction();
		et.begin();
		Query query = em.createQuery(queryString);
		resultList = (List<Playlist>) query.getResultList();	
		et.commit();
		return resultList;
		}
		catch(Exception e) {
		    if(e != null) {
			et.rollback();
		}
		    throw e;
		}
		    finally {
		    	em.close();
		    }
		
	} 

	
	/**
	 * removeSongOfAllPlaylists
	 * 
	 * removes a song from all playlists used in the playlist "AllSongs"
	 * 
	 * @param song
	 */
	public static void removeSongOfAllPlaylists (int songId){
		List<Playlist> toDeleteFrom = Database.showAllPlaylists();
		for (Playlist selectedPlaylist : toDeleteFrom) {
			Database.deleteSongFromPlaylist(selectedPlaylist.getPlaylistId(), songId);
		}
		Database.deleteSong(songId);
		
	}
	
	
	/**
	 * sortPlaylist
	 * 
	 * updates the way a playlist is sorted
	 * 
	 * @param playlistId (the playlist which will be sorted)
	 * @param sortStatus (the sortstatus which will be used on the chosen palylist)
	 */
	public static void sortPlaylist(int playlistId, int sortStatus) {
		EntityManager em = ENTITY_MANAGER_FACTORY.createEntityManager();
		EntityTransaction et = null;
	    final Playlist result;
		final String queryString = "SELECT p FROM Playlist p WHERE p.playlistId = " + playlistId;
		try {
		et = em.getTransaction();
		et.begin();
		Query query = em.createQuery(queryString);
		result = (Playlist) query.getSingleResult();
		result.updateSorting(sortStatus);
		em.persist(result);
		et.commit();
		}
		catch(Exception e) {
		    if(e != null) {
			et.rollback();
		}
		    throw e;
		}
		    finally {
		    	em.close();
		    }
		
	}
	
	
	/**
	 * setSongTitle
	 * 
	 * allows to set the title of a song
	 * 
	 * @param title (new title)
	 * @param songId (the song of which the title is going to be changed)
	 */
	public static void setSongTitle(String title, int songId) throws MP3PlayerException {
		EntityManager em = ENTITY_MANAGER_FACTORY.createEntityManager();
		EntityTransaction et = null;
		Song songToBeChanged;
		final String queryString = "SELECT s FROM Song s WHERE s.songId = " + songId;
		try {
			et = em.getTransaction();
			et.begin();
			Query query = em.createQuery(queryString);
			songToBeChanged = (Song) query.getSingleResult();
			if(!songToBeChanged.getTitle().equals(title)) {
			    songToBeChanged.setTitle(title);
			    em.persist(songToBeChanged);
			}
			et.commit();	
		}
		catch(MP3PlayerException e) {
		    if(e != null) {
			et.rollback();
		    }
		    throw e;
		}
		    finally {
		    	em.close();
		    }
	}
	
	
	/**
	 * setSongArtist
	 * 
	 * allows to set the artist of a song
	 * 
	 * @param artist (new artist)
	 * @param songId (the song of which the artist is going to be changed)
	 */
	public static void setSongArtist(String artist, int songId) throws MP3PlayerException {
		EntityManager em = ENTITY_MANAGER_FACTORY.createEntityManager();
		EntityTransaction et = null;
		final String queryString = "SELECT s FROM Song s WHERE s.songId = " + songId;
		Song songToBeChanged;
		try {
			et = em.getTransaction();
			et.begin();
			Query query = em.createQuery(queryString);
			songToBeChanged = (Song) query.getSingleResult();
			if(!songToBeChanged.getArtist().equals(artist)) {
			    songToBeChanged.setArtist(artist);
			    em.persist(songToBeChanged);
			}
			et.commit();	
		}
		catch(MP3PlayerException e) {
		    if(e != null) {
			et.rollback();
		}
		    throw e;
		}
		    finally {
		    	em.close();
		    }
	}
	
	
	/**
	 * setSongAlbum
	 * 
	 * allows to set the album of a song
	 * 
	 * @param album (new album)
	 * @param songId (the song of which the album is going to be changed)
	 */
	public static void setSongAlbum(String album, int songId) throws MP3PlayerException {
		EntityManager em = ENTITY_MANAGER_FACTORY.createEntityManager();
		EntityTransaction et = null;
		final String queryString = "SELECT s FROM Song s WHERE s.songId = " + songId;
		Song songToBeChanged;
		try {
			et = em.getTransaction();
			et.begin();
			Query query = em.createQuery(queryString);
			songToBeChanged = (Song) query.getSingleResult();
			if(!songToBeChanged.getAlbum().equals(album)) {
			   songToBeChanged.setAlbum(album);
			   em.persist(songToBeChanged);
			}
			et.commit();	
		}
		catch(MP3PlayerException e) {
		    if(e != null) {
			et.rollback();
		}
		    throw e;
		}
		    finally {
		    	em.close();
		    }
	}
}
		













	

