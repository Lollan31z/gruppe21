package org.prog3.musicplayer2021.Exceptions;

/**
 * ExceptionClass for InitiateFile
 * @author pascal ney
 */
public class InitiateFileException extends MP3PlayerException {
	
	public InitiateFileException(String msg) {
		super(msg);
	}
}
