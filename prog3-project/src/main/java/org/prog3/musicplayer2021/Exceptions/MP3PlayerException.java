package org.prog3.musicplayer2021.Exceptions;


/**
 * super class for all mp3-player related exceptions
 * @author pascal ney
 *
 */
public class MP3PlayerException extends Exception {
	public MP3PlayerException(String msg) {
		super(msg);
	}
}
