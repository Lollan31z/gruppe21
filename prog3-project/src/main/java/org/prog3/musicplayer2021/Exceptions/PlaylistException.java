package org.prog3.musicplayer2021.Exceptions;

/**
 * ExceptionClass for Playlist
 * @author pascal ney
 *
 */
public class PlaylistException extends MP3PlayerException {
	
	public PlaylistException(String msg) {
		super("PlaylistException: " + msg);
	}

}
