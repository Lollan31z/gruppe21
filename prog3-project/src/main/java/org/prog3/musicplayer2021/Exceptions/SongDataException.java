package org.prog3.musicplayer2021.Exceptions;

/**
 * ExceptionClass for SongData
 * @author pascal ney
 *
 */
public class SongDataException extends MP3PlayerException {
	public SongDataException(String msg) {
		super("SongDataException: " + msg);
	}
}
