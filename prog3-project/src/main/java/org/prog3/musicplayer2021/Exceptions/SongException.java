package org.prog3.musicplayer2021.Exceptions;

/**
 * ExceptionClass for Song
 * @author pascal ney
 *
 */
public class SongException extends MP3PlayerException {
	
	public SongException(String errorMsg) {
		super(errorMsg);
	}

}
