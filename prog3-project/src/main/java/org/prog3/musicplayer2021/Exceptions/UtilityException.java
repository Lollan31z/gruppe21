package org.prog3.musicplayer2021.Exceptions;

/**
 * ExceptionClass for Utility
 * @author pascal ney
 *
 */
public class UtilityException extends MP3PlayerException {
	
	public UtilityException(String msg) {
		super(msg);
	}

}
