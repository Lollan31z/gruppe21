package org.prog3.musicplayer2021.Functions;

import java.io.IOException;
import java.util.List;
import org.prog3.musicplayer2021.Database.*;
import org.prog3.musicplayer2021.Exceptions.*;
import org.prog3.musicplayer2021.Interfaces.GUI_Interface;

/**
 * This class operates as an interface between gui and database
 * @author pascal ney
 *
 */

public class GUIFunctions implements GUI_Interface{
	
	/**
	 * Initiates the Path to the directory where all songs are saved.
	 * @param path Path to the directory with the songs.
	 * @throws IOException
	 */
	@Override
	public void initiateFileToSongs(String path) throws IOException {
		InitiateFile.initiateDirectoryPath(path);
	}
	
	
	/**
	 * Returns the dirPath which is specified in an txt.
	 * @return the Path to the directory with the saved Songs.
	 * @throws IOException
	 * @throws InitiateFileException 
	 */
	@Override
	public String getDirPathToSongs() throws IOException, MP3PlayerException {
		return InitiateFile.getDirPathToSongs();
	}
	
	/**
	 * Checks if the path to the directory is already instantiated
	 * @return true if the path exists, false if not.
	 * @throws IOException
	 * @throws MP3PlayerException 
	 */
	@Override
	public  boolean dirPathExists() throws IOException, MP3PlayerException {
		return InitiateFile.dirPathExists();
	}
	
	/**
	 * Finds songs in the Database by their primaryKey
	 * @param primaryKey
	 * @return
	 */
	@Override
	public Song findSongInDatabaseByPrimaryKey(final int primaryKey) {
		return Database.findSongByPrimaryKey(primaryKey);
	}
	
	/**
	 * Returns a list of songs which have the same titles.
	 * 
	 * @param title - Title of Songs
	 * @return The List of Songs with same titles.
	 */
	@Override
	public List<Song> findSongInDatabaseByTitle(final String title) {
		return Database.findSongByTitle(title);
	}
	
	/**
	 * Finds a Song in the Database by its path.
	 * @param path
	 * @return The Song with the given path
	 */
	@Override
	public  Song findSongInDatabaseByPath(final String path) {
		return Database.findSongByPath(path);
	}
	
	/**
	 * Finds Songs by the name of the artist.
	 * @param artist
	 * @return List of Songs with given artist.
	 */
	@Override
	public  List<Song> findSongsInDatabaseByArtist(final String artist) {
		return Database.findSongsByArtist(artist);
	}


	/**
	 * Finds Songs by their albumname
	 * @param albumName
	 * @return List of Songs with the given albumname
	 */
	@Override
	public List<Song> findSongsInDatabaseByAlbumName(final String albumName){
		return Database.findSongsByAlbumName(albumName);
	}
	
	/**
	 * Returns all songs in the database as a list.
	 * @return The list with all Songs
	 */
	@Override
	public List<Song> findAllSongsInDatabaseAsList(){
		return Database.findAllSongs();
	}
	
	/**
	 * Returns the count of all Songs in the database.
	 * @return the song count
	 */
	@Override
	public int countAllSongsInDatabase() {
		return Database.findAllSongs().size();
	}
	
	/**
	 * Returns the count of all playlists in the database.
	 * @return the playlist count
	 */
	@Override
	public int countAllPlaylistsInDatabase() {
		return Database.showAllPlaylists().size();
	}
	
	/**
	 * Returns all songs in the Database as playlist
	 * @return Playlist of all songs in the Database
	 * @throws MP3PlayerException
	 */
	@Override
	public Playlist findAllSongsInDatabaseAsPlaylist() throws MP3PlayerException {
		return Database.findAllSongsAsPlaylist();
	}
	
	/**
	 * Deletes a song from the database and the filesystem.
	 * @param songId
	 * @throws Exception
	 */
	@Override
	public void deleteSongFromDatabase(final int songId) throws MP3PlayerException, IOException {
		if(SongData.removeFile(Database.findSongByPrimaryKey(songId).getPath())) {
		   Database.deleteSong(songId);
		}
		else {
			throw new SongDataException("Song could not be deleted!");
		}
	}
	
	/**
	 * Returns all songs in a playlist as a list.
	 * @param playlistId - PlaylistId of the playlist
	 * @return
	 */
	@Override
	public List<Song> showSongsInPlaylist(Playlist temp){
		int playlistId = temp.getPlaylistId();
		return Database.showSongsInPlaylist(playlistId);
	}
	
	/**
	 * Returns a list of all playlists in the database.
	 * @return list of all playlist in the database.
	 */
	@Override
	public List<Playlist> showAllPlaylists(){
		return Database.showAllPlaylists();
	}
	
	/**
	 * Add Song to Playlist with database call
	 * @param songId, playlistname
	 */
	@Override
	public void addSongToPlaylistGuiDB (int songId, String playlistname) throws MP3PlayerException{
		Database.addSongToPlaylist(songId, Database.findPlaylistByName(playlistname).getPlaylistId());
	}
	/**
	 * checks if playlist name exists in database
	 * @return Playlist
	 */
	@Override
	public boolean findPlaylistByNameGuiDB (String playlistname) throws MP3PlayerException{
		if(Database.findPlaylistByName(playlistname) == null) {
			return false;
		} else {
			return true;
		}
	}
	/**
	 * add new Playlist to DB
	 */
	@Override
	public void addPlaylistGuiDB (String playlistname) throws MP3PlayerException{
		Database.addPlaylist(new Playlist(playlistname));
	}
	/**
	 * remove Playlist from DB
	 */
	@Override
	public void removePlaylistGuiDB (String playlistname) throws MP3PlayerException{
		Database.deletePlaylist(Database.findPlaylistByName(playlistname).getPlaylistId());

	}
	/**
	 * checks if playlist name exists in database
	 * @return Playlist
	 * @throws IOException 
	 */
	@Override
	public void removeSongFromPlaylistGuiDB (String playlistname,int songId) throws MP3PlayerException, IOException{
		if(playlistname.equals("AllSongs")) {
			SongData.removeFile(Database.findSongByPrimaryKey(songId).getPath());
			Database.removeSongOfAllPlaylists(songId);
		}else {
		Database.deleteSongFromPlaylist(Database.findPlaylistByName(playlistname).getPlaylistId(),songId);
		}
	}
	/**
	 * checks if db has song with name tosearch
	 * @param tosearch
	 * @return List<Song>
	 */
	@Override
	public List<Song> searchName (String tosearch){
		return Database.findSongByTitle(tosearch);
				
	}
	/**
	 * checks if db has song with Artist tosearch
	 * @param tosearch
	 * @return List<Song>
	 */
	@Override
	public List<Song> searchArtist (String tosearch){
		return Database.findSongsByArtist(tosearch);
				
	}
	/**
	 * checks if db has song with Album tosearch
	 * @param tosearch
	 * @return List<Song>
	 */
	@Override
	public List<Song> searchAlbum (String tosearch) {
		return Database.findSongsByAlbumName(tosearch);
				
	}	
	
	/**
	 * Sorts the playlist by the given sortStatus
	 * @param playlist
	 * @param sortStatus
	 */
	@Override
	public void sortPlaylistInDatabase(Playlist playlist, int sortStatus) {
		Database.sortPlaylist(playlist.getPlaylistId(), sortStatus);
	}

	/**
	 * Changes the title of the given song
	 * @param title
	 * @param songId
	 * @throws MP3PlayerException
	 */
	@Override
	public void setSongTitle(String title, int songId) throws MP3PlayerException{
		Database.setSongTitle(title, songId);
		
	}
	
	/**
	 * Changes the album of the given song
	 * @param album
	 * @param songId
	 * @throws MP3PlayerException
	 */
	@Override
	public void setSongAlbum(String album, int songId) throws MP3PlayerException {
		Database.setSongAlbum(album, songId);
		
	}
	
	/**
	 * Changes the artist of the given song
	 * @param artist
	 * @param songId
	 * @throws MP3PlayerException
	 */
	@Override
	public void setSongArtist(String artist, int songId) throws MP3PlayerException {
		Database.setSongArtist(artist, songId);
		
	}

}
