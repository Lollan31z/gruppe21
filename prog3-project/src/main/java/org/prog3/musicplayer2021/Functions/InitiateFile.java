package org.prog3.musicplayer2021.Functions;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.prog3.musicplayer2021.Exceptions.InitiateFileException;

/**
 * This class initiates the txt-file, in which the filepath to the songs is saved.
 * @author pascal ney
 *
 */
public class InitiateFile{
private static final String TXT_PATHNAME = "DirectoryPath.txt";
private static final String ERR_TXT_EMPTY = "The filepath has not been initiated yet!";
private static final String ERR_TXT_NOT_FOUND = "The file \"" + TXT_PATHNAME + "\" could not be found! Please choose a file first!";
	
	
/**
 * Writes the given path into a txt-file.
 * @param path
 * @throws IOException
 */
	public static void initiateDirectoryPath(String path) throws IOException{
		StringBuffer sb = new StringBuffer(path);
		String os = System.getProperty("os.name").toLowerCase();
		if (os.contains("win")){
		    //OS is Windows-based
			sb.append("\\");
			
		}
		else if (os.contains("osx")){
		    //OS is Apple OSX 
			sb.append("\\");
		}      
		else if (os.contains("nix") || os.contains("aix") || os.contains("nux")){
		    //OS is Linux/Unix based
			sb.append("/");
		}
		String fullPath = new String(sb);
        Files.write(Paths.get(TXT_PATHNAME), fullPath.getBytes());
    }
    
	
	/**
	 * Reads 
	 * @return
	 * @throws IOException
	 * @throws InitiateFileException 
	 */
    public static String getDirPathToSongs() throws IOException, InitiateFileException{
    	if(!txtFileExists()) {
    		throw new InitiateFileException(ERR_TXT_NOT_FOUND);
    	}
          String path = Files.readString(Paths.get(TXT_PATHNAME));
          if(path.isEmpty()) {
        	  throw new InitiateFileException(ERR_TXT_EMPTY);
          }
          return path;
    }
    
    public static boolean dirPathExists() throws IOException, InitiateFileException {
    	return new File(getDirPathToSongs()).exists();
    }
    
    /**
     * Checks if the txt-file with the directory path exists
     * @return true, if yes, false if no.
     */
    public static boolean txtFileExists() {
    	return new File(TXT_PATHNAME).exists();
    }
    
}