package org.prog3.musicplayer2021.Functions;

import java.util.*;
import org.prog3.musicplayer2021.Exceptions.PlaylistException;
import org.prog3.musicplayer2021.Exceptions.SongException;

/**
 * This class is used to iterate through playlists
 * @author hendrik blacha
 *
 */
public class PlayThisSong
{
	
	private Song currentSong;
	private List<Song> currentPlaylist;
	private ListIterator<Song> playlistIterator; 
	
	private static final String SONG_CAN_NOT_BE_NULL = "The song must not be null";
	private static final String PLAYLIST_CAN_NOT_BE_NULL = "The playlist must not be null";
	private final static String SONG_NOT_IN_PLAYLIST = "The song is not in playlist";
	
	/**
	 * sets the currentPlaylist
	 * @param playlist
	 * @throws PlaylistException
	 */
	public void setCurrentPlaylist(Playlist playlist) throws PlaylistException
	{
		if(playlist.getSonglist() == null) 
		{
			throw new PlaylistException(PLAYLIST_CAN_NOT_BE_NULL);
		}
		else 
		{
			this.currentPlaylist = playlist.getSonglist();
			this.playlistIterator = this.currentPlaylist.listIterator();
		}
	}

	/**
	 * sets the currentSong
	 * @param song
	 * @throws SongException
	 */
	public void setCurrentSong(Song song) throws SongException
	{
		if(currentPlaylist.contains(song)) 
		{
			if(song == null) 
			{
				throw new SongException(SONG_CAN_NOT_BE_NULL);
			}
			else 
			{
				this.currentSong = song;
				int index = this.currentPlaylist.indexOf(this.currentSong);
				this.playlistIterator = this.currentPlaylist.listIterator(index);
			}
		}
		else 
		{
			throw new SongException(SONG_NOT_IN_PLAYLIST);
		}
	}

	//if raus?
	/**
	 * returns the currentSong 
	 * @return this.currentSong
	 * @throws SongException
	 */
	public Song getCurrentSong() throws SongException
	{
		if(this.currentSong == null)
        {
            throw new SongException(SONG_CAN_NOT_BE_NULL);
        }
        else
        {
            return this.currentSong;
        }
	}
	
	//return this.playlistIterator.next() ?
	/**
	 * returns the next song in currentPlaylist
	 * @return playlistIterator.next()
	 * @return this.playlistIterator.next()
	 * @throws SongException
	 */
	public Song getNewNextSong() throws SongException
	{
		if(playlistIterator == null)
		{
			throw new SongException(PLAYLIST_CAN_NOT_BE_NULL);
		}
		else
		{
			if (this.playlistIterator.hasNext())
			{
				return this.playlistIterator.next();
			}
			
			this.playlistIterator = this.currentPlaylist.listIterator();
		
			return this.playlistIterator.next();
		}	
	}
	
	/**
	 * returns the previous song in currentPlaylist
	 * @return this.playlistIterator.previous()
	 * @throws SongException
	 */
	public Song getNewPreviousSong() throws SongException
	{
		if(playlistIterator == null)
		{
			throw new SongException(PLAYLIST_CAN_NOT_BE_NULL);
		}
		else
		{
			if (this.playlistIterator.hasPrevious())
			{
				return this.playlistIterator.previous();
			}
			while (this.playlistIterator.hasNext())
			{
				this.playlistIterator.next();
			}
			return this.playlistIterator.previous();
		}
	}
	
	/**
	 * randomizes the currentPlaylist
	 * @return this.currentSong
	 * @throws PlaylistException
	 */
	public Song randomizePlaylist() throws PlaylistException
	{
		if(currentPlaylist == null)
		{
			throw new PlaylistException(PLAYLIST_CAN_NOT_BE_NULL);
		}
		else
		{
			Collections.shuffle(this.currentPlaylist);
			this.playlistIterator = this.currentPlaylist.listIterator();
			this.currentSong = this.playlistIterator.next();
			return this.currentSong;		
		}
	}
	
	/**
	 * sets the iterator of playThisSong
	 * @param id
	 * @return
	 */
	public Song setIteratorToSong(int id)
	{
		this.playlistIterator = this.currentPlaylist.listIterator(id);
		this.currentSong = this.playlistIterator.next();
		return currentSong;
	}

	//should get deleted
	//gets maybe deleted
	//call this method on startup or before a new playlist will be played
	public Song updatePlaylist(Playlist newPlaylist) throws PlaylistException
	{
		if (this.currentPlaylist != null)
		{
			this.currentPlaylist.clear();
		}
		
		setCurrentPlaylist(newPlaylist);
		this.playlistIterator =this.currentPlaylist.listIterator();
		this.currentSong = this.playlistIterator.next();
		return this.currentSong;
	}
	
	/**
	 * updates the next song in currentPlaylist
	 * currentSong gets repeated if the currentPlaylist is one 
	 * @return this.currentSong
	 * @throws PlaylistException
	 * @throws SongException
	 */
	public Song updateNextSong() throws PlaylistException, SongException
	{
		if(currentPlaylist== null)
		{
			throw new PlaylistException(PLAYLIST_CAN_NOT_BE_NULL);
		}
		else
		{
			Song tempSong = getNewNextSong();
			if (tempSong.equals(this.currentSong))
			{
				tempSong = getNewNextSong();
			}
			this.currentSong = tempSong;
			return this.currentSong;
		}
	}

	/**
	 * updates the previous song in currentPlaylist
	 * currentSong gets repeated if the currentPlylist is one 
	 * @return this.currentSong
	 * @throws SongException
	 * @throws PlaylistException
	 */
	public Song updatePreviousSong() throws SongException, PlaylistException
	{
		if(currentPlaylist== null)
		{
			throw new PlaylistException(PLAYLIST_CAN_NOT_BE_NULL);
		}
		else
		{
			Song tempSong = getNewPreviousSong();
			if (tempSong.equals(this.currentSong))
			{
				tempSong = getNewPreviousSong();
			}
			this.currentSong = tempSong;
			return this.currentSong;
		}
	}
}
