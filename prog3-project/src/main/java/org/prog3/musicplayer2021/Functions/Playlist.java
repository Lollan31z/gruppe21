package org.prog3.musicplayer2021.Functions;

import java.util.*;
import javax.persistence.*;
import org.prog3.musicplayer2021.Exceptions.PlaylistException;

/**
 * This class represents a playlist in the mp3-player
 * @author pascal ney
 *
 */

@Entity
@Table(name = "Playlists")
public class Playlist{
	
	//Errormessages
	private static final String ERROR_PLAYLISTNAME_EMPTY = "Playlistname must not be empty!";
	private static final String ERROR_SONG_IS_NULL = "Song must not be null!";
	private static final String ERROR_PATH_IS_EMPTY = "Path must not be null!";
	
	//Sortstati
	private static final int SORT_BY_TITLE = 1;
	private static final int SORT_BY_ARTIST = 2;
	private static final int SORT_BY_ALBUM = 3;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "playlistId", updatable = false, nullable = false)
	private int playlistId;
	@Column(name = "playlistName")
	private String playlistName;
	private int sortStatus;

	@ManyToMany(fetch=FetchType.EAGER)
	@JoinTable(name = "isin", 
	           joinColumns= @JoinColumn(name = "SongId"),
	        		  inverseJoinColumns = @JoinColumn(name ="PlaylistId"))
	private List<Song> songList;
	
	public Playlist() {
	}
	
	/**
	 * Constructor for Playlist instanciates a Playlist and sets sortStatus
	 * to byTitle
	 * @param playlistName - The name of the playlist
	 * @throws PlaylistException - If playlistName is null or empty
	 */
	public Playlist(String playlistName) throws PlaylistException {
		if(playlistName == null || playlistName.isEmpty()) {
			throw new PlaylistException(ERROR_PLAYLISTNAME_EMPTY);
		}
		setPlaylistName(playlistName);
		songList = new LinkedList<Song>();
		songList.sort(Utility.compareSongsByTitle());
		sortStatus = SORT_BY_TITLE;
	}
	
	/**
	 * Constructor for test. Here the playlist is not initially sorted.
	 * @param playlistName
	 * @param offset
	 * @throws PlaylistException
	 */
	public Playlist(String playlistName, int offset) throws PlaylistException {
		if(playlistName == null || playlistName.isEmpty()) {
			throw new PlaylistException(ERROR_PLAYLISTNAME_EMPTY);
		}
		setPlaylistName(playlistName);
		songList = new LinkedList<Song>();
		//songList.sort(Utility.compareSongsByTitle());
		//sortStatus = SORT_BY_TITLE;
	}
	

	
	
	/**
	 * Method sortByTitle sorts the internal songList alphabetically by the 
	 * titles of the songs. 
	 */
	public void sortByTitle() {
		songList.sort(Utility.compareSongsByTitle());
		sortStatus = SORT_BY_TITLE;
	}
	
	/**
	 * Method getIndex returns the index of the entered 
	 * song inside the playlist by comparing paths.
	 * @param song1 - The song you want to know the index of.
	 * @return index of the song, if it exists.
	 */
	public int getIndexOfSong(Song song1) throws PlaylistException {
		if(song1.equals(null)) {
			throw new PlaylistException(ERROR_SONG_IS_NULL);
		}
		for(int i = 0; i < songList.size(); i++) {
			if (song1.getPath().equals(songList.get(i).getPath())) {
				return i;
			}
		}
		return -1;
	}
		
	
	/**
	 * Method sortByAlbum sorts the Songs in the Playlist alphabetically by their albumnames.
	 */
	public void sortByAlbum() {
		songList.sort(Utility.compareSongsByAlbum());
		sortStatus = SORT_BY_ALBUM;
	}
	
	/**
	 * Method sortByAlbum sorts the Songs in the Playlist alphabetically by their albumnames.
	 */
	public void sortByArtist() {
		songList.sort(Utility.compareSongsByArtist());
		sortStatus = SORT_BY_ARTIST;
	}
	
	/**
	 * Method addSongToPlaylist adds the 
	 * given song to the playlist.
	 * @param song - The given song.
	 * @throws PlaylistException if the given song has value null.
	 */
	public void addSongToPlaylist(Song song) throws PlaylistException {
		if(song == null) {
			throw new PlaylistException(ERROR_SONG_IS_NULL);
		}
		songList.add(song);
		updateSorting(sortStatus);
	}
	
	/**
	 * Method removeSongByPath finds a song by its path and removes this song.
	 * @param path - The path of the song to be removed.
	 * @return true, if the song is found and removed. 
	 *         false, if the song is not found.
	 * @throws PlaylistException - If the path is null or empty.
	 */
	public boolean removeSongByPath(String path) throws PlaylistException {
		if(path == null || path.isEmpty()) {
			throw new PlaylistException(ERROR_PATH_IS_EMPTY);
		}
		boolean removed = false;
		Song song = findSongByPath(path);
		removed = songList.remove(song);
		updateSorting(sortStatus);
		return removed;
	}
	
	/**
	 * Method findSongByPath finds a song in the playlist by its path.
	 * @param path - The path of the song
	 * @return Song
	 * @throws PlaylistException if the path to the song does not exist.
	 */
	public Song findSongByPath(String path) throws PlaylistException {
		
		if(path == null || path.isEmpty()) {
			throw new PlaylistException(ERROR_PATH_IS_EMPTY);
		}
		for(Song song: songList) {
			if(song.getPath().equals(path)) {
				return song;
			}
		}
		throw new PlaylistException("Song to path \'" + path + "\' does not exist!");
	}
	
	

	/**
	 * Method updateSorting sorts the playlist after the sortstatus.
	 */
	public void updateSorting(int sortStatus) {
		if(sortStatus == SORT_BY_TITLE) {
			sortByTitle();
		}
		else if(sortStatus == SORT_BY_ALBUM) {
			sortByAlbum();
		}
		else if(sortStatus == SORT_BY_ARTIST){
			sortByArtist();
		}
	
	
	}

	/**
	 * Method getPlaylistName returns the name of the playlist.
	 * @return playlistName.
	 */
	public String getPlaylistName() {
		return playlistName;
	}
	
	/**
	 * Method setPlaylistName sets the name of the playlist to the given parameter.
	 * @param playlistName
	 */
	public void setPlaylistName(String playlistName) {
		this.playlistName = playlistName;
	}

	/**
	 * Method getPlaylistId returns the ID of the playlist.
	 * @return playlistId
	 */
	public int getPlaylistId() {
		return playlistId;
	}
	

	/**
	 * Method getSortStatus returns the current sortstatus. 
	 * @return sortStatus
	 */
	public int getSortStatus() {
		return sortStatus;
	}
	
	/**
	 * Method getSongList returns the list of songs inside the playlist.
	 * @return songList
	 */
	public List<Song> getSonglist(){
		return songList;
	}
	
	/**
	 * Method getSortStatus setSortStatus sets the 
	 * sortstatus to the given sortstatus.
	 * @param sortStatus
	 */
	public void setSortStatus(int sortStatus) {
	   this.sortStatus = sortStatus;	
	}
	
	
	/**
	 * Method getSize returns the count of songs in the playlist.
	 * @return
	 */
	public int getSize() {
		return songList.size();
	}
	

	/**
	 * Returns a String representation of Playlist
	 * @return String representation of Playlist
	*/
	
	public String toString() {
		return getPlaylistName();
	}
	
	/**
	 * Returns a song on the index 'index' of playlist
	 */
	
	public Song getSongByIndex(int index) throws PlaylistException {
		if(index < 0 || index > songList.size() - 1) {
			throw new PlaylistException("Index zu hoch oder negativ!");
		}
		return songList.get(index);
	}

	


}
