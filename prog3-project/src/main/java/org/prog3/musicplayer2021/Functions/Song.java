package org.prog3.musicplayer2021.Functions;

import java.io.IOException;
import javax.persistence.*;
import org.prog3.musicplayer2021.Exceptions.*;


/**
 * This class represents a song in the mp3-player
 * @author pascal ney, paul kiel
 *
 */
@Entity
@Table(name = "Songs")
public class Song{
	private static String ALBUM_UNKNOWN = "unknown";
	private static String MP3 = ".mp3";
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "songId", updatable = false, nullable = false)
	private int songId;
	@Column(name = "title")
	private String title;
	@Column(name = "artistname")
	private String artist;
	@Column(name = "path")
	private String path;
	//private int duration;
	@Column(name = "albumname")
	private String album;
	//private Media songMedia;
	

	/**
	 * Song Song is a constructor of the class Song with no arguments
	 * the empty constructor is needed for database
	 */
	public Song() {
		
	}

	/**
	 * Song song is a constructor of the class Song with 4 parameters
	 * Parameters: String title, String artist, String path, String album
	 * @param title
	 * @param artist
	 * @param path
	 * @param album
	 * @throws SongException
	 * @throws IOException
	 */
	public Song(String title, String artist, String path, String album) throws SongException, IOException {
		setTitle(title);
		setArtist(artist);
		setPath(path);
		setAlbum(album);
	}

	/**
	 * Song Song is a constructor of the class Song with 3 parameters
	 * constructor that sets album default to unknown
	 * @param title
	 * @param artist
	 * @param path
	 * @throws SongException
	 * @throws IOException
	 */
	public Song(String title, String artist, String path ) throws SongException, IOException {
		setTitle(title);
		setArtist(artist);
		setPath(path);
		this.album = ALBUM_UNKNOWN;
	}
	
	/**
	 * @return songId
	 */
	public int getSongId() {
		return songId;
	}
	
	/**
	 * @return artist;
	 */
	public String getArtist() {
		return artist;
	}
	
	/**
	 * @return path
	 */
	public String getPath() {
		return path;
	}

	/**
	 * @return album
	 */
	public String getAlbum() {
		return album;
	}
	
	/**
	 * @return title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * sets songId to this.songId
	 */
	public void setSongId(int songId){
		this.songId = songId;
	}

	/**
	 * sets artist to this.artist if artist is not empty or null
	 * @param artist
	 * @throws SongException
	 */
	public void setArtist(String artist) throws SongException {
		if(artist == null || artist.isEmpty()) {	
			throw new SongException("Artist must not be empty!");
		}
		this.artist = artist;
	}

	/**
	 * sets path to this.path if path is not empty or null
	 * @param path
	 * @throws SongException
	 */
	public void setPath(String path) throws SongException{
		if(path == null || path.isEmpty()) {
			throw new SongException("The path can not be empty");
		} else if(path.endsWith(MP3)) {
			this.path = path;
		} else {
			throw new SongException("The path must end with .mp3!");
		}
	}

	/**
	 * sets album to this.album if album is not empty or null
	 * sets album to unknown if album is null or empty
	 * @param album
	 * @throws SongException
	 */
	public void setAlbum(String album) throws SongException {
		if(album == null || album.isEmpty()) {
			throw new SongException("The album is not known.");
		}
		this.album = album;
	}

	/**
	 * sets title to this.title
	 * @param title
	 * @throws SongException
	 */
	public void setTitle(String title) throws SongException {
		if(title == null || title.isEmpty()) {
			throw new SongException("The song has to have a title");
		}
		this.title = title;
	}
	
	/**
	 * @return getTitle() + getArtist() + getAlbum()
	 */
	public String toString() {
		return getTitle() + "\n" + getArtist() + " [" +  getAlbum() + "]";
		
	}
	
	/**
	 * returns a String for GUI
	 * @return getTitle() + getArtist() + getAlbum()
	 */
	public String toStringForGUI() {
		return getTitle() + " - " + getArtist() + " [" +  getAlbum() + "]";
		
	}

	/**
	 * returns a hashcode
	 * @return 
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((album == null) ? 0 : album.hashCode());
		result = prime * result + ((artist == null) ? 0 : artist.hashCode());
		result = prime * result + ((path == null) ? 0 : path.hashCode());
		result = prime * result + songId;
		result = prime * result + ((title == null) ? 0 : title.hashCode());
		return result;
	}

	/**
	 * overrides the equals method
	 * @param obj
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Song other = (Song) obj;
		if (album == null) {
			if (other.album != null)
				return false;
		} else if (!album.equals(other.album))
			return false;
		if (artist == null) {
			if (other.artist != null)
				return false;
		} else if (!artist.equals(other.artist))
			return false;
		if (path == null) {
			if (other.path != null)
				return false;
		} else if (!path.equals(other.path))
			return false;
		if (songId != other.songId)
			return false;
		if (title == null) {
			if (other.title != null)
				return false;
		} else if (!title.equals(other.title))
			return false;
		return true;
	}
	
}
