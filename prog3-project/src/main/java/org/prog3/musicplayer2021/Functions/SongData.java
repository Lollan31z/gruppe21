package org.prog3.musicplayer2021.Functions;

import org.prog3.musicplayer2021.Database.Database;
import org.prog3.musicplayer2021.Exceptions.*;
import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * This class is used to read songs from the files and put them into the database,
 * or to delete them
 * @author pascal ney
 */
public class SongData {
	
	/**
	 * Runs through the file with Songs and adds Songs which are not already in the Database to it,
	 * while removing songs from the Database which are not in the filesystem anymore.
	 * @throws Exception
	 */
	public void refreshSongs() throws MP3PlayerException, IOException {
		if(Database.showAllPlaylists().size() == 0 || Database.findPlaylistByName("AllSongs") == (null)){
			Database.addPlaylist(Database.findAllSongsAsPlaylist());
		}
		checkIfSongFileDirExists();
		//Database.addPlaylist(Database.findAllSongsAsPlaylist());
        File[] files = new File(InitiateFile.getDirPathToSongs()).listFiles();
        for(File file: files) {
            String filePath = file.getPath();
            filePath = Utility.getRelativePath(filePath);            
            if(Utility.pathEndsWithMP3(filePath) && !isInDatabase(filePath)){
            	Database.addSong(new Song(Utility.getTitleByPath(filePath), Utility.getArtistByPath(filePath), filePath));
            	Database.addLatestSongToPlaylist();
            }
        }
        for(Song song: Database.findAllSongs()) {
        	if(!isInFolder(song.getPath())) {
        		Database.deleteSong(song.getSongId());
        	}
        }
        
	}

	
	/**
	 * Removes a file from the file-system
	 * @param path
	 * @return true, if the file could be removed, false if not.
	 * @throws InitiateFileException
	 * @throws IOException
	 * @throws SongDataException
	 */
	public static boolean removeFile(String path) throws InitiateFileException, IOException, SongDataException {
		String fullPath = new String(new StringBuffer(
				InitiateFile.getDirPathToSongs()).append(path));
		File toBeDeleted = new File(fullPath);
		if(toBeDeleted.exists()) {
			return toBeDeleted.delete();
		}
		else {
			throw new SongDataException("The file '" + fullPath + "' does not exist!");
		}
	}
	
	/**
	 * Checks if a song with path path already exists in the database.
	 * @param path - path of the song to be checked.
	 * @return
	 */
	private boolean isInDatabase(String path) {
		List<Song> allSongs = Database.findAllSongs();
		for(Song song: allSongs) {
			if(song.getPath().equals(path)) {
				return true;
			}		
		}
		return false;
	}
	
	/**
	 * Checks if a file with the path path exists in the folder.
	 * @param path - path fo the song-file
	 * @return
	 * @throws IOException
	 * @throws SongDataException
	 * @throws UtilityException 
	 * @throws InitiateFileException 
	 */
	private boolean isInFolder(String path) throws IOException, SongDataException, UtilityException, InitiateFileException {
		checkIfSongFileDirExists();
        File[] files = new File(InitiateFile.getDirPathToSongs()).listFiles();
        for(File file: files) {
        	String filePath = file.getPath();
        	filePath = Utility.getRelativePath(filePath);
        	if(filePath.equals(path)) {
        		return true;
        	}
        }
        return false;
        
	}
	
	
	/**
	 * Checks if the folder exists.
	 * @throws IOException
	 * @throws SongDataException
	 * @throws InitiateFileException 
	 */
	private static void checkIfSongFileDirExists() throws IOException, SongDataException, InitiateFileException {
		boolean fileCreated = new File(InitiateFile.getDirPathToSongs()).exists();
        if(!fileCreated) {
            throw new SongDataException("The Directory \"" + InitiateFile.getDirPathToSongs() + "\"does not exist!");
        }
	}
	
}
