package org.prog3.musicplayer2021.Functions;
//ey
import java.util.Comparator;

import org.prog3.musicplayer2021.Exceptions.UtilityException;


/**
 * This class contains utility-functions for the classes Playlist and SongData
 * @author pascal ney
 *
 */
public class Utility {
	
	//Error-Messages
	private static final String ERR_PATH_NULL = "The path must not be null!";
	private static final String ERR_PATH_NOT_ABS = "Path is not absolute!";
	private static final String ERR_ENDS_WITH_MP3 = "Path must end with .mp3!";
	private static final String UNTITLED = "Untitled";
	private static final String UNKNOWN_ARTIST = "Various Artists";
	private static final String ERR_PATH_DIR = "Path must not be a directory!";
	
	
	/**
	 * Generates a comparator which sorts the songs alphabetically descending by their title.
	 * @return The Comparator
	 */
	public static Comparator<Song> compareSongsByTitle(){
		return new Comparator<Song>() {

			@Override
			public int compare(Song s1, Song s2) {
				return s1.getTitle().compareToIgnoreCase(s2.getTitle());
			}
			
		};
	}
	
	/**
	 * Generates a comparator which sorts the songs alphabetically descending by their album-name.
	 * @return The Comparator
	 */
	public static Comparator<Song> compareSongsByAlbum(){
		return new Comparator<Song>() {

			@Override
			public int compare(Song s1, Song s2) {
				return s1.getAlbum().compareToIgnoreCase(s2.getAlbum());
			}
			
		};
	}
	
	/**
	 * Generates a comparator which sorts the songs alphabetically descending by their artist-name.
	 * @return The Comparator
	 */
	public static Comparator<Song> compareSongsByArtist(){
		return new Comparator<Song>() {

			@Override
			public int compare(Song s1, Song s2) {
				return s1.getArtist().compareToIgnoreCase(s2.getArtist());
			}
			
		};
	}
	
	
	
	/**
	 * Checks if the given path references a mp3-file
	 * @param path the path to be checked
	 * @return true, if it is a mp3-file, false if not.
	 * @throws UtilityException if path is null.
	 */
	public static boolean pathEndsWithMP3(String path) throws UtilityException {
		checkPathNull(path);
		return path.matches(".*\\.mp3");
	}
	
	/**
	 * Reads the artist from the path in form "title - artist.mp3"
	 * @param path the given path
	 * @return the artist-name, if found, UNKNOWN_ARTIST, if not found.
	 * @throws UtilityException if the path is null or no mp3-path
	 */
	public static String getArtistByPath(String path) throws UtilityException {
		checkEndsWithMP3(path);
		checkPathNull(path);
		if(!path.matches(".*-.*")) {
			return UNKNOWN_ARTIST;
		}
		String[] sArray = path.split("-");
	       if(sArray.length == 2){
	           String artist = sArray[1].substring(0, sArray[1].indexOf(".mp3")).trim();
	           if(artist.equals("")) {
	        	   return UNKNOWN_ARTIST;
	           }
	           else return artist;
	       }
	       else if(sArray.length >= 2) {
	    	   StringBuffer artistSb = new StringBuffer();
	    	   for(int i = 0; i < sArray.length - 1; i++) {
	    		   artistSb = artistSb.append("-").append(sArray[i+1]);
	    	   };
	    	   return new String(artistSb).substring(0, artistSb.indexOf(".mp3")).trim();
	       }
	       return UNKNOWN_ARTIST;
	}
	
	/**
	 * Reads the artist from the path in form "title - artist.mp3"
	 * @param path the given path
	 * @return the title, if found, UNTITLED, if not found.
	 * @throws UtilityException if the path is null or no mp3-path
	 */
	public static String getTitleByPath(String path) throws UtilityException{
		checkEndsWithMP3(path);
		checkPathNull(path);
		if(!path.matches(".*-.*")) {
			return path.substring(0, path.indexOf(".mp3")).trim();
		}
		String[] sArray = path.split("-");
        if(sArray.length > 0){
            String title = sArray[0].trim();
            if(title.equals("")) {
            	return UNTITLED;
            }
            else {
            	return title;
            }
        }
        return "Untitled";
	}
	
	/**
	 * Reads the relative path from a absolute path.
	 * @param path - the absolute path
	 * @return relativePath - the relative path.
	 * @throws UtilityException if the path is not absolute or a directory.
	 */
	public static String getRelativePath(String path) throws UtilityException {
		checkPathNull(path);
		String relativePath;
        if(path.contains("\\") || path.contains("/")){
        	if(path.lastIndexOf("\\") > path.lastIndexOf("/")) {
                 relativePath = path.substring(path.lastIndexOf("\\") + 1);
        	}
        	else {
        		relativePath = path.substring(path.lastIndexOf("/") + 1);
        	}
        	if(relativePath.isEmpty()) {
             	throw new UtilityException(ERR_PATH_DIR);
             }
        	return relativePath;
             
        }
       
        else {
        	throw new UtilityException(ERR_PATH_NOT_ABS);
        }	
        
    }
	
	/**
	 * Checks if a given path has value null
	 * @param path
	 * @throws UtilityException if the value of path is null.
	 */
	private static void checkPathNull(String path) throws UtilityException {
		if(path == null) {
			throw new UtilityException(ERR_PATH_NULL);
		}
	}
	
	/**
	 * Checks if a path ends with ".mp3".
	 * @param path
	 * @throws UtilityException if path doesn't end with ".mp3".
	 */
	private static void checkEndsWithMP3(String path) throws UtilityException {
		if(!pathEndsWithMP3(path)) {
			throw new UtilityException(ERR_ENDS_WITH_MP3);
		}
	}
}
	
	