package org.prog3.musicplayer2021.GUI.application;



import javafx.stage.*;
import org.prog3.musicplayer2021.Exceptions.MP3PlayerException;
import org.prog3.musicplayer2021.Functions.GUIFunctions;
import org.prog3.musicplayer2021.Functions.Song;
import org.prog3.musicplayer2021.GUI.languageSettings.ButtonClass;
import org.prog3.musicplayer2021.GUI.languageSettings.TitelClass;
import javafx.scene.*;
import javafx.scene.layout.*;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;

/**
 * the class ChangeSongMeta opens a window which allows changing Song Meta Data
 * 
 * @author pascal ney, fabian weinland, daniel gliemmo
 * @version 1
 *
 */
public class ChangeSongMeta{
	
	private static final String SCENE_FILL = "-fx-background-color: #5c5c5c;";
	private static final String BUTTON_FILL = "#2e2e2e";
	private static final String TEXT_FIELD_STYLE = "-fx-text-inner-color: cyan;";
	private static final String TEXT_STYLE = "-fx-font-weight: bold;";
	private static Button btnChangeMetadata;
	private static Stage windowChangeMeta;
	private static Scene sceneChangeMeta;
	private static TextField textfieldTitle;
	private static TextField textfieldArtist;
	private static TextField textfieldAlbum;
	private static VBox layoutChangeMeta;
	private static GUIFunctions guifunctions = new GUIFunctions();
	private static Button buttonQuit;
	private static TitelClass title;
	private static ButtonClass button2;

	
	

	/**
	 * Sets stage for the ChangeSongMeta-Window
	 * @param songId
	 * @throws MP3PlayerException
	 */
	public static void changeSongMeta(int songId) throws MP3PlayerException {
		Song songToBeChanged = guifunctions.findSongInDatabaseByPrimaryKey(songId);
		title = new TitelClass();
		button2 = new ButtonClass();
		windowChangeMeta = new Stage();
		windowChangeMeta.initModality(Modality.APPLICATION_MODAL); //priority window
		windowChangeMeta.initStyle(StageStyle.UTILITY);
		windowChangeMeta.setTitle("Change Metadata of \"" + songToBeChanged.toStringForGUI() + "\"");
		
		layoutChangeMeta = new VBox(14);
		layoutChangeMeta.setStyle(SCENE_FILL);
		
		buttonQuit = new Button("Quit");
	    buttonQuit.setBackground(new Background(new BackgroundFill(Color.valueOf(BUTTON_FILL), null, null)));
	    buttonQuit.setTextFill(Color.CYAN);
	    buttonQuit.setStyle(TEXT_STYLE);
		buttonQuit.setOnAction(e -> windowChangeMeta.close());
		
		Text textTitle = new Text("Songtitle");
		textTitle.setStyle(TEXT_STYLE);
		textTitle.setFill(Color.CYAN);
		textfieldTitle = new TextField();
		textfieldTitle.setPrefWidth(350);
		textfieldTitle.setMaxWidth(350);
		textfieldTitle.setAlignment(Pos.CENTER);
		textfieldTitle.setText(songToBeChanged.getTitle());
		textfieldTitle.setStyle(TEXT_FIELD_STYLE);
		textfieldTitle.setBackground(new Background(new BackgroundFill(Color.valueOf(BUTTON_FILL),null, null)));
		
		Text textArtist = new Text("Artist");
		textArtist.setStyle(TEXT_STYLE);
		textArtist.setFill(Color.CYAN);
		textfieldArtist = new TextField();
		textfieldArtist.setPrefWidth(350);
		textfieldArtist.setMaxWidth(350);
		textfieldArtist.setAlignment(Pos.CENTER);
		textfieldArtist.setText(songToBeChanged.getArtist());
		textfieldArtist.setStyle(TEXT_FIELD_STYLE);
		textfieldArtist.setBackground(new Background(new BackgroundFill(Color.valueOf(BUTTON_FILL),null, null)));
		
		Text textAlbum = new Text("Album");
		textAlbum.setStyle(TEXT_STYLE);
		textAlbum.setFill(Color.CYAN);
		textfieldAlbum = new TextField();
		textfieldAlbum.setPrefWidth(350);
		textfieldAlbum.setMaxWidth(350);
		textfieldAlbum.setAlignment(Pos.CENTER);
		textfieldAlbum.setText(songToBeChanged.getAlbum());
		textfieldAlbum.setStyle(TEXT_FIELD_STYLE);
		textfieldAlbum.setBackground(new Background(new BackgroundFill(Color.valueOf(BUTTON_FILL),null, null)));
		
		btnChangeMetadata = new Button("Change Metadata");
		btnChangeMetadata.setBackground(new Background(new BackgroundFill(Color.valueOf(BUTTON_FILL), null, null)));
		btnChangeMetadata.setTextFill(Color.CYAN);
		btnChangeMetadata.setStyle(TEXT_STYLE);
		btnChangeMetadata.setOnAction(e -> {
			
			try {
			guifunctions.setSongTitle(textfieldTitle.getText(), songId);	
			guifunctions.setSongArtist(textfieldArtist.getText(), songId);
			guifunctions.setSongAlbum(textfieldAlbum.getText(), songId);
			windowChangeMeta.close();
			} catch (MP3PlayerException e1) {
				ExceptionWindow.show(e1);
			}	
		
			
		});						
		sceneChangeMeta = new Scene(layoutChangeMeta,500,400);
		layoutChangeMeta.getChildren().addAll(textTitle, textfieldTitle, textArtist, textfieldArtist, textAlbum, textfieldAlbum, btnChangeMetadata, buttonQuit);
		layoutChangeMeta.setAlignment(Pos.CENTER);		
		windowChangeMeta.setScene(sceneChangeMeta);
		windowChangeMeta.setMaxHeight(400);
		windowChangeMeta.setMaxWidth(500);
		windowChangeMeta.setMinHeight(400);
		windowChangeMeta.setMinWidth(500);
		windowChangeMeta.showAndWait(); 
		
	}

	
}
