package org.prog3.musicplayer2021.GUI.application;

import org.prog3.musicplayer2021.Exceptions.MP3PlayerException;

import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
/**
 * ExceptionWindow displays Exceptions in the GUI
 * @author pascal n
 *
 */
public class ExceptionWindow {
	private static String EXCEPTION_TEXT_STYLE = "-fx-font-weight: bold; -fx-background-color: #2e2e2e;";
    private static Stage windowShowException;
	private static Text exceptionText;
	private static VBox layoutShowException;
	private static Scene sceneShowException;
	public static void show(MP3PlayerException e) {
		exceptionText = new Text(e.getMessage());
		exceptionText.setFill(Color.CYAN);
		exceptionText.setStyle(EXCEPTION_TEXT_STYLE);
		windowShowException = new Stage();
		windowShowException.initModality(Modality.APPLICATION_MODAL);
		windowShowException.initStyle(StageStyle.UTILITY);
		windowShowException.setTitle("Warning!");
		layoutShowException = new VBox();
		layoutShowException.setStyle("-fx-background-color: #5c5c5c;");
		sceneShowException = new Scene(layoutShowException,500,200);
		layoutShowException.getChildren().add(exceptionText);
		layoutShowException.setAlignment(Pos.CENTER);
		windowShowException.setMaxHeight(200);
		windowShowException.setMinHeight(200);
		windowShowException.setMaxWidth(500);
		windowShowException.setMinWidth(500);
		windowShowException.setScene(sceneShowException);
		windowShowException.showAndWait();
	}

}
