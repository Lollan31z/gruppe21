package org.prog3.musicplayer2021.GUI.application;
	
import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.fxml.FXMLLoader;


/**
 * Main creates the main window by loading resources, calls class MainWindowController
 * @author fabian weinland, daniel gliemmo
 *
 */
public class Main extends Application { 
	
	
	
	
	private Stage windowMediaPlayer;
	private Scene sceneMediaPlayer;
	private Parent rootFXMLFile;
	private FXMLLoader loaderFXMLMediaPlayer;
	private static final String CSS_PATH = "/org/prog3/musicplayer2021/GUI/application/MainstyleSheet.css";
	private static final String FXML_PATH = "MP3Window1.fxml";
	private static final String WINDOW_NAME = "Musicplayer 2021";

	
	/**
	 * Sets the stage for our main-Window
	 */
	@Override
	public void start(Stage primaryStage){
		try {
			windowMediaPlayer = primaryStage;
			primaryStage.setMinHeight(600);
			primaryStage.setMinWidth(900);
			loaderFXMLMediaPlayer = new FXMLLoader(getClass().getResource(FXML_PATH));
			rootFXMLFile = loaderFXMLMediaPlayer.load();
			windowMediaPlayer.setTitle(WINDOW_NAME);
			windowMediaPlayer.setOnCloseRequest(e-> {
					windowMediaPlayer.close();

			});
			
			sceneMediaPlayer = new Scene(rootFXMLFile,900,600);
			sceneMediaPlayer.getStylesheets().add(CSS_PATH);
			windowMediaPlayer.setScene(sceneMediaPlayer);
			MainWindowController mainWindowController = loaderFXMLMediaPlayer.getController();
			mainWindowController.setMain(this);
			String css = MainWindowController.class.getResource(CSS_PATH).toExternalForm();
			sceneMediaPlayer.getStylesheets().add(css);
			windowMediaPlayer.show();
		
		} 
		
		catch(Exception e) {
			e.printStackTrace();
		}
		
	}
	
	public static void main(String[] args) {
		launch(args);
	}
}
