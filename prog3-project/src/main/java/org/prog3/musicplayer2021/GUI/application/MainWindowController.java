package org.prog3.musicplayer2021.GUI.application;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import org.prog3.musicplayer2021.Exceptions.MP3PlayerException;
import org.prog3.musicplayer2021.Exceptions.PlaylistException;
import org.prog3.musicplayer2021.Functions.*;
import java.io.File;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.stage.*;
import org.prog3.musicplayer2021.GUI.languageSettings.*;

import javafx.scene.control.*;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.media.*;
import javafx.scene.paint.Color;

/**
 * the class MainWindowController handles the buttons on Main window
 * 
 * @author fabian weinland, daniel gliemmo, pascal ney
 * @version 1
 *
 */
public class MainWindowController implements Initializable{
	
	private static final String PLAYBUTTON_STYLE_PAUSE = 
			"-fx-shape: \"M 250 350 L 250 450 L 350 450 L 350 350 "
			+ "Z\";; -fx-rotate: 90; -fx-background-color: #2e2e2e; -fx-border-width: 1; -fx-border-color: cyan;";
	private static final String PLAYBUTTON_STYLE_PLAY = 
			"-fx-shape: \"M 150 0 L75 200 L225 200 "
			+ "Z\";; -fx-rotate: 90; -fx-background-color: #2e2e2e; -fx-border-width: 1; -fx-border-color: cyan;";
	private static final String PLAYBUTTON_STYLE_PLAY_PRESSED ="-fx-shape: \"M 150 0 L75 200 L225 200 "
			+ "Z\";; -fx-rotate: 90; -fx-background-color: black; -fx-border-width: 1; -fx-border-color: cyan;";
	private static final String PLAYBUTTON_STYLE_PAUSE_PRESSED = "-fx-shape: \"M 250 350 L 250 450 L 350 450 L 350 350 "
			+ "Z\";; -fx-rotate: 90; -fx-background-color: black; -fx-border-width: 1; -fx-border-color: cyan;";
	private static final String BUTTON_PRESSED = "-fx-background-color: #2e2e2e; -fx-border-width: 1; -fx-border-color: cyan;";
	private static final String BUTTON_RELEASED = "-fx-background-color: #2e2e2e;";
	private static final String FX_COLOR_CYAN = "-fx-background-color: CYAN";
	private static final String SONG_NOT_SELECTED = "Please select a song first!";
	private static final String PLAYLIST_NOT_SELECTED = "Please select a playlist first!";
	private static final String FILE_NOT_KNOWN = "Choose a File!";
	private static final String NO_SUCH_SONG = "No such song!";
	private static final int BY_TITLE = 1;
	private static final int BY_ARTIST = 2;
	private static final int BY_ALBUM = 3;
	private static final String NOT_REMOVE_ALLSONGS = "AllSongs cannot be removed!";
	private static int selectedPlaylist;
	private static boolean playing = false;
	MediaPlayer mediaPlayer;
	public Main main;
	public static String languageString;
	private static ButtonClass buttonClass;
	private static Song currentSong,selectedSong;
	private Playlist currentPlaylist;
	private PlayThisSong playThisSong = new PlayThisSong();
	private boolean isAutoPlay = false;
	private GUIFunctions guifunctions = new GUIFunctions();
	private SongData songdata = new SongData();
	private boolean repeat;
	private boolean random = false;
	
	@FXML
	Label labelPath = new Label();
	@FXML
	Button playButton = new Button();
	@FXML
	Button changeMetadata = new Button();
	@FXML
	MenuItem languageMenuItem = new MenuItem();
	@FXML
	MenuButton sortSongs = new MenuButton();
	@FXML
	Button sortByTitle = new Button();
	@FXML
	Button sortByArtist = new Button();
	@FXML
	Button sortByAlbum = new Button();
	@FXML
	Button addMusicBtn = new Button();
	@FXML
	Button newPlaylistBtn = new Button();
	@FXML
	Menu fileMenu = new Menu();
	@FXML
	TextField searchField = new TextField();
	@FXML
	Menu editMenu = new Menu();
	@FXML 
	Menu helpMenu = new Menu();
	@FXML
	TextField songText = new TextField();
	@FXML
	ObservableList<Playlist> playlistSingly = FXCollections.observableArrayList();
	@FXML
	ObservableList<Song> songsSingly = FXCollections.observableArrayList();
	@FXML
	ObservableList<Song> searchList = FXCollections.observableArrayList();
	@FXML
	ListView<Playlist> playlistsView = new ListView<Playlist>(playlistSingly);
	@FXML
	ListView<Song> songsView = new ListView<Song>(songsSingly);
	@FXML
	private Button removeSongBtn = new Button();
	@FXML
	private Button removePlaylistBtn = new Button();
	
	
	public void setMain(Main main) {
		this.main = main;
	}
	
	
	/**
	 * initialize Listview with playlists by calling loadPlaylists
	 * and set currentSong/selectedSong = null
	 */
    @Override
	public void initialize(URL url, ResourceBundle rb) {
	    try {
	    	repeat = false;
			loadPlaylists();	
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		currentSong = null;
		songsView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
	}
    
	/**
	 * loadPlaylists to initialize the list of playlists in listview
	 * @throws MP3PlayerException 
	 */
	public void loadPlaylists() {
			playlistSingly.setAll(guifunctions.showAllPlaylists());
			playlistsView.setItems(playlistSingly);
	}
	
	/**
	 * loadSongs to load a listview filled with songs when selected Playlist by user
	 * by calling loadSongsFromPlaylist(playlist)
	 * @throws MP3PlayerException 
	*/
	public void handleLoadSongs() {
		try {
			Playlist temp = playlistsView.getSelectionModel().getSelectedItem();
			if(temp == null) {
				throw new MP3PlayerException(PLAYLIST_NOT_SELECTED);
			}
			currentPlaylist = temp;
			playThisSong.setCurrentPlaylist(currentPlaylist);
			loadSongsFromPlaylist(temp);
		}   catch (MP3PlayerException e1) {
				ExceptionWindow.show(e1);
		}
	}
	/**
	 * loadSongsFromPlaylist to load all songs from selected playlist
	 */
	public void loadSongsFromPlaylist(Playlist playlist) {
		songsSingly.setAll(guifunctions.showSongsInPlaylist(playlist));
		songsView.setItems(songsSingly);
		
	}
	/**
	 * loadSongsFromAllPlaylists to load all songs from database as "AllSongs" Playlist
	 */
	public void loadSongsFromAllPlaylists() {
		for(Playlist playlist: playlistSingly) {
			loadSongsFromPlaylist(playlist);
		}
	}
	/**
	 * handleSelectSong to set currentSong and selectedSong
	 * @throws MP3PlayerException 
	 * @throws IOException  
	 */
	public void handleSelectSong(MouseEvent event) {
		try {
			selectedSong = songsView.getSelectionModel().getSelectedItem();
			if(event.getButton().equals(MouseButton.PRIMARY)){
				if(event.getClickCount() == 2){
					playSong(selectedSong);
					songText.setText(selectedSong.getTitle());
					playThisSong.setCurrentSong(selectedSong);
					currentSong = selectedSong;
				}
			
				
			}       
			else if(event.getButton() == MouseButton.SECONDARY) {
					handleChangeSongMeta();
					for(Playlist playlist: playlistSingly) {
						if(playlist.getSortStatus() == BY_TITLE) {
							guifunctions.sortPlaylistInDatabase(playlist, BY_TITLE);
						}
						else if(playlist.getSortStatus() == BY_ARTIST) {
							guifunctions.sortPlaylistInDatabase(playlist, BY_ARTIST);
						}
						else if(playlist.getSortStatus() == BY_ALBUM) {
							guifunctions.sortPlaylistInDatabase(playlist, BY_ALBUM);
						}
						loadSongsFromPlaylist(playlist);
					}
					loadSongsFromPlaylist(playlistsView.getSelectionModel().getSelectedItem());				
					loadPlaylists();
			}
		}
		catch(MP3PlayerException e) {
			ExceptionWindow.show(e);
		}
		catch(Exception e) {
			e.printStackTrace();
		}
			

	}
				
    
	/**
	 * isSongSelected to check if there is a selected song
	 * @return true or false
	 */
	public boolean isSongSelected(){
		if (currentSong != null) {
			return true;
		} else {
			return false;
		}
	}
	/**
	 *handleSearch to handle searchfield
	 * 
	 */
	public void handleSearch(){
			String searchstring = searchField.getText().trim();
			searchList.addAll(guifunctions.searchName(searchstring));
			searchList.addAll(guifunctions.searchArtist(searchstring));
			searchList.addAll(guifunctions.searchAlbum(searchstring));
			if (searchList != null) {
				songsView.setItems(searchList);
			}
		
	}
	/**
	 *handleSortSongsByTitle to sort songs in selected playlist by title
	 * 
	*/
	public void handleSortSongsByTitle(){
		try {
		
			Playlist selected = currentPlaylist;
			if(selected == null) {
				throw new MP3PlayerException(PLAYLIST_NOT_SELECTED);
			}
			else if(selected.getSortStatus() != BY_TITLE) {
		    	guifunctions.sortPlaylistInDatabase(selected, BY_TITLE);
			    loadSongsFromPlaylist(selected);
			}
			handleRefresh();
		}
		catch(MP3PlayerException e) {
			ExceptionWindow.show(e);
		}
		
	}
		

	/**
	 *handleSortSongsByArtist to sort songs in selected playlist by Artist 
	 *  
	*/
	public void handleSortSongsByArtist(){
		try {
			Playlist selected = currentPlaylist;
			if(selected == null) {
				throw new MP3PlayerException(PLAYLIST_NOT_SELECTED);
			}
			else if(selected.getSortStatus() != BY_ARTIST) {
				guifunctions.sortPlaylistInDatabase(selected, BY_ARTIST);
				loadSongsFromPlaylist(selected);
			}
			handleRefresh();
		}
		catch(MP3PlayerException e) {
			ExceptionWindow.show(e);
		}
	}

	/**
	 *handleSortSongsByAlbum to sort songs in selected playlist by Album 
	 * 
	*/
	public void handleSortSongsByAlbum() {
		try {
			Playlist selected = currentPlaylist;
			if(selected == null) {
				throw new MP3PlayerException(PLAYLIST_NOT_SELECTED);
			}
			else if(selected.getSortStatus() != BY_ALBUM) {
				guifunctions.sortPlaylistInDatabase(selected, BY_ALBUM);
				loadSongsFromPlaylist(selected);
			}
			
			handleRefresh();
		}
		catch(MP3PlayerException e) {
			ExceptionWindow.show(e);
		}
			
	}
	
	/**
	 *handleAddMusic handle click on addMusic button
	 * 
	 */
	public void handleAddMusic(){
		try {
			List<Song> selectedItems = songsView.getSelectionModel().getSelectedItems();
				if(selectedItems != null && selectedItems.size() > 0) {
					WindowAddMusicFXML.launch(selectedItems);
				}
				else {
					throw new MP3PlayerException(SONG_NOT_SELECTED);
				}
				
			
		}
		catch(MP3PlayerException e) {
			ExceptionWindow.show(e);
		}
		catch(IOException e) {
			e.printStackTrace();
		}
		finally {
			handleRefresh();
		}
	}
	
	/**
	 * handleRefresh actualize playlists after changes 
	 */
	public void handleRefresh(){
		try {
			int current = playlistsView.getSelectionModel().getSelectedIndex();
			if(current == -1) {
				current = 0;
			}
			songdata.refreshSongs();
			loadSongsFromAllPlaylists();
			loadPlaylists();
			labelPath.setText(InitiateFile.getDirPathToSongs());
			playlistsView.getSelectionModel().select(current);
			handleLoadSongs();
		} catch (MP3PlayerException e1) {
			ExceptionWindow.show(e1);
		}
		catch(IOException e) {
			e.printStackTrace();
		}
	}
		
	
	/**
	 * handleAddPlaylist handle click on addPlaylist
	 *  
	 */
	public void handleAddPlaylist(){
		try {
			WindowAddPlaylistFXML.launch();
		}
		catch(MP3PlayerException e) {
			ExceptionWindow.show(e);
		}
		catch(IOException e) {
			e.printStackTrace();
		}
		finally {
			handleRefresh();
		}
	}
	
	/**
	 * handleRemoveMusic handle click on removeSong
	 * @throws IOException 
	 * @throws MP3PlayerException 
	*/
	public void handleRemoveMusic() throws MP3PlayerException, IOException {
		try {
			selectedPlaylist = playlistsView.getSelectionModel().getSelectedIndex();
			List<Song> songs = songsView.getSelectionModel().getSelectedItems();
			if (songs != null && songs.size() > 0) {
				if(currentPlaylist.getPlaylistName().equals("AllSongs")) {
					WindowRemoveMusicAllSongs.launch(songs);
					
				}
				else {
					WindowRemoveMusicFXML.launch(songs, currentPlaylist);
					
				}
			}
			
			else {
				throw new MP3PlayerException(SONG_NOT_SELECTED);
			}
			
		}
		catch(MP3PlayerException e) {
			ExceptionWindow.show(e);
		}
		finally {
			playlistsView.getSelectionModel().select(selectedPlaylist);
			loadPlaylists();
			handleRefresh();
		}
		
	}
	
	/**
	 * handleRemoveMusic handle click on removePlaylist
	 * @throws IOException 
	 * @throws MP3PlayerException 
	*/
	public void handleRemovePlaylist() {
		try {
			Playlist removePL = playlistsView.getSelectionModel().getSelectedItem();
			if (removePL!= null) {
				if(removePL.getPlaylistName().equals("AllSongs")) {
					throw new MP3PlayerException(NOT_REMOVE_ALLSONGS);
				}
				WindowRemovePlaylist.removePlaylist(removePL);
			}
			else {
				throw new MP3PlayerException(PLAYLIST_NOT_SELECTED);
			}
			
		}
		catch(MP3PlayerException e) {
			ExceptionWindow.show(e);
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		finally {
			loadPlaylists();
			handleRefresh();
		}
	}
	/**
	 * handleLanguage handle click on Language
	 */
	public void handleLanguage() {
		buttonClass = new ButtonClass();
		WindowChangeLanguage.changeLanguage();
		languageMenuItem.setText(buttonClass.buttonLanguage());
		addMusicBtn.setText(buttonClass.buttonAddMusic());
		newPlaylistBtn.setText(buttonClass.buttonNewPlaylist());
		fileMenu.setText(buttonClass.menuFile());
		editMenu.setText(buttonClass.menuEdit());
		helpMenu.setText(buttonClass.menuHelp());
	}
	
	/**
	 * handlePlay handle click on PlayButton (play or pause)
	 * @throws IOException 
	 * @throws MP3PlayerException 
	 */
	public void handlePlay(){
		try {
			if(this.isAutoPlay) {	
				mediaPlayer.pause();
				playing = false;
				playButton.setStyle(PLAYBUTTON_STYLE_PLAY);
				this.isAutoPlay = false;
			}else if(isSongSelected()) {
				songsView.getSelectionModel().clearAndSelect(getIndexOfSong(songsView, currentSong));
				mediaPlayer.play();
				playing = true;
				playButton.setStyle(PLAYBUTTON_STYLE_PAUSE);
				isAutoPlay = true;
				currentSong = playThisSong.getCurrentSong();
			}
		}
		catch(MP3PlayerException e) {
			ExceptionWindow.show(e);
		}
	}
	
	/**
	 * handleNext handle click on Next
	 * plays the next song
	 * @throws IOException
	 * @throws MP3PlayerException
	 */
	public void handleNext(){
		try {
			if(!repeat) {
				currentSong = playThisSong.updateNextSong();
			}
			songText.setText(currentSong.getTitle());
			playSong(currentSong);
		}
		catch(MP3PlayerException e) {
			ExceptionWindow.show(e);
		}
	
	}
	
	/**
	 * handlePrevious handle click on Prev
	 * plays previous song
	 * @throws IOException 
	 * @throws MP3PlayerException 
	 */
	public void handlePrevious(){
		try {
			currentSong = playThisSong.updatePreviousSong();
			playSong(currentSong);
		}
		catch(MP3PlayerException e) {
			ExceptionWindow.show(e);
		}
	}
	
	/**
	 * playSong starts playing a song
	 * @param song 
	 * @throws IOException 
	 * @throws MP3PlayerException 
	 */
	private void playSong(Song song) {
		try {
			if(isAutoPlay) {
				playButton.setStyle(PLAYBUTTON_STYLE_PLAY);
				playButton.setStyle(FX_COLOR_CYAN);
				mediaPlayer.stop();
				playing = false;
			}
			currentSong = song;
			File file = new File(InitiateFile.getDirPathToSongs() + song.getPath());
			this.mediaPlayer = new MediaPlayer(new Media(file.toURI().toString()));
			this.isAutoPlay = true;
			mediaPlayer.play();
			playing = true;
			songsView.getSelectionModel().clearAndSelect(getIndexOfSong(songsView, currentSong));
			if(random) {
				songsView.scrollTo(getIndexOfSong(songsView,currentSong));
			}
			playButton.setStyle(PLAYBUTTON_STYLE_PAUSE);
			mediaPlayer.setOnEndOfMedia(new Runnable(){
				@Override 
				public void run() {
					if(repeat) {
						playSong(currentSong);
					
					}else {
						handleNext();
					}
				
				}
			});
		}
		catch(MP3PlayerException e) {
			ExceptionWindow.show(e);
		}
		catch(IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * handleFile defines the directory with songs 
	 * @throws IOException 
	 * @throws MP3PlayerException 
	 */
	public void handleFile(){
		try {
			Stage primaryStage = new Stage();
			primaryStage.setTitle(FILE_NOT_KNOWN);
			DirectoryChooser directoryChooser = new DirectoryChooser();
			String path = directoryChooser.showDialog(primaryStage).getAbsolutePath();
			labelPath.setText(path);
			InitiateFile.initiateDirectoryPath(path);
			handleRefresh();
		}
		catch(IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * handleChangeSongMeta defines the directory with songs 
	 * @throws MP3PlayerException 
	 */
	public void handleChangeSongMeta() {
		try {
			Song songToBeChanged = songsView.getSelectionModel().getSelectedItem();
			ChangeSongMeta.changeSongMeta(songToBeChanged.getSongId());
			for(Playlist playlist: playlistSingly) {
				loadSongsFromPlaylist(playlist);
			}
		}
		catch(MP3PlayerException e) {
			ExceptionWindow.show(e);
		}
	}
	
	/**
	 * sets the value for repeat.
	 */
	public void handleRepeat() {
		if(repeat) {
			repeat = false;
		}else {
			repeat = true;
		}
	}
	/**
	 * handleRandom handles click on random button
	 * @throws PlaylistException 
	 */
	public void handleRandom() throws PlaylistException {
		if(random) {
			random = false;
		}
		else {
			random = true;
		}
		playThisSong.randomizePlaylist();
	}
	
	/**
	 * Changes the buttoncolor upon changing sortStatus
	 */
	public void handleButtonColor() {
		if(playlistsView.getSelectionModel().getSelectedItem() == null) {
			sortByTitle.setTextFill(Color.CYAN);
			sortByArtist.setTextFill(Color.CYAN);
			sortByAlbum.setTextFill(Color.CYAN);
		}
		else if(playlistsView.getSelectionModel().getSelectedItem().getSortStatus() == BY_TITLE) {
			sortByTitle.setStyle(BUTTON_PRESSED);
			sortByArtist.setStyle(BUTTON_RELEASED);
			sortByAlbum.setStyle(BUTTON_RELEASED);
		}
		else if(playlistsView.getSelectionModel().getSelectedItem().getSortStatus() == BY_ARTIST) {
			sortByTitle.setStyle(BUTTON_RELEASED);
			sortByArtist.setStyle(BUTTON_PRESSED);
			sortByAlbum.setStyle(BUTTON_RELEASED);
		}
		else if(playlistsView.getSelectionModel().getSelectedItem().getSortStatus() == BY_ALBUM) {
			sortByTitle.setStyle(BUTTON_RELEASED);
			sortByArtist.setStyle(BUTTON_RELEASED);
			sortByAlbum.setStyle(BUTTON_PRESSED);
		}
	}
	
	/**
	 * Changes the color of the button-border when pressed
	 */
	public void handleChangeBtnColorAddMusicReleased() {
		addMusicBtn.setStyle(BUTTON_RELEASED);
	}
	
	/**
	 * Changes the color of the button-border when released
	 */
	public void handleChangeBtnColorAddMusicPressed() {
		addMusicBtn.setStyle(BUTTON_PRESSED);
	}
	
	/**
	 * Changes the color of the button-border when released
	 */
	public void handleChangeBtnColorRemoveMusicReleased() {
		removeSongBtn.setStyle(BUTTON_RELEASED);
	}
	
	/**
	 * Changes the color of the button-border when pressed
	 */
	public void handleChangeBtnColorRemoveMusicPressed() {
		removeSongBtn.setStyle(BUTTON_PRESSED);
	}
	
	/**
	 * Changes the color of the button-border when pressed
	 */
	public void handleChangeBtnColorAddPlaylistReleased() {
		newPlaylistBtn.setStyle(BUTTON_RELEASED);
	}
	
	/**
	 * Changes the color of the button-border when pressed
	 */
	public void handleChangeBtnColorAddPlaylistPressed() {
		newPlaylistBtn.setStyle(BUTTON_PRESSED);
	}
	
	/**
	 * Changes the color of the button-border when released
	 */
	public void handleChangeBtnColorRemovePlaylistReleased() {
		removePlaylistBtn.setStyle(BUTTON_RELEASED);
	}
	
	/**
	 * Changes the color of the button-border when pressed
	 */
	public void handleChangeBtnColorRemovePlaylistPressed() {
		removePlaylistBtn.setStyle(BUTTON_PRESSED);
	}
	
	
	
	/**
	 * getIndexOfSong to find the index of a given song in the given LisView
	 * @param view
	 * @param song
	 * @throws MP3PlayerException
	 */
	private int getIndexOfSong(ListView<Song> view, Song song) throws MP3PlayerException {
		for(int i = 0; i < view.getItems().size(); i++) {
			if(view.getItems().get(i).getSongId() == song.getSongId()) {
				return i;
			}
		}
		throw new MP3PlayerException(NO_SUCH_SONG);
	}
	
	
	/**
	 * Returns the index of the selected song in songsSingly
	 * @return the index
	 */
	public static int getSelectedSongId() {
		return selectedSong.getSongId();
	}
	
	/**
	 * Changes the color of the Play-Button when pressed
	 */
	public void handlePlayPressed() {
		if(playing) {
			playButton.setStyle(PLAYBUTTON_STYLE_PAUSE_PRESSED);
		}
		else {
			playButton.setStyle(PLAYBUTTON_STYLE_PLAY_PRESSED);
		}
	}
	
	/**
	 * Changes the color of the Play-Button when released
	 */
	public void handlePlayReleased() {
		if(playing) {
			playButton.setStyle(PLAYBUTTON_STYLE_PAUSE);
		}
		else {
			playButton.setStyle(PLAYBUTTON_STYLE_PLAY);
		}
	}
	
	
}
	
	
	

