package org.prog3.musicplayer2021.GUI.application;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import org.prog3.musicplayer2021.Exceptions.MP3PlayerException;
import org.prog3.musicplayer2021.Functions.GUIFunctions;
import org.prog3.musicplayer2021.Functions.Song;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 * WindowAddMusicFXML handles the window for adding music to playlists
 * @author pascal ney, fabian weinland, daniel gliemmo
 *
 */
public class WindowAddMusicFXML {
	private static final String NO_SUCH_PLAYLIST = "There is no such playlist!";
	private static final String NO_ADDING_ALL_SONGS = "You cannot add to 'AllSongs'!";
	private static final String FXML_PATH = "WindowAddMusic.fxml";
	private static final String CSS_PATH = "/org/prog3/musicplayer2021/GUI/application/MainStylesheet.css";
	
	@FXML
	private TextField textFieldAddSong = new TextField();
	private static List<Song> songsToBeAdded;
	private GUIFunctions guifunctions = new GUIFunctions();
	private static Stage stage;
	private static Scene sceneAddMusic;
	
	
	/**
	 * Launches the window for adding songs to playlists
	 * @param songs - list of songs to be added.
	 * @throws IOException
	 */
	public static void launch(List<Song> songs) throws IOException {
		songsToBeAdded = new LinkedList<Song>();
		songsToBeAdded.addAll(songs);
		FXMLLoader fxmlLoader = new FXMLLoader(WindowAddMusicFXML.class.getResource(FXML_PATH));
	    Parent root = (Parent) fxmlLoader.load();
	    stage = new Stage();
	    stage.initModality(Modality.APPLICATION_MODAL);
	    stage.initStyle(StageStyle.UTILITY);
	    //stageAttr.initStyle(StageStyle.UNDECORATED);
	    stage.setTitle("Please enter a playlist");
	    sceneAddMusic = new Scene(root, 550, 300);
	    sceneAddMusic.getStylesheets().add(CSS_PATH);
	    stage.setScene(sceneAddMusic);
	    stage.setMaxHeight(335);
	    stage.setMaxWidth(570);
	    stage.setMinHeight(335);
	    stage.setMinWidth(570);
	    stage.show();
	}
	
	
	/**
	 * Adds songs to the given playlist
	 * @throws MP3PlayerException
	 */
	public void handleAddSong() throws MP3PlayerException {
		if (guifunctions.findPlaylistByNameGuiDB(textFieldAddSong.getText()) && 
				!textFieldAddSong.getText().equals("AllSongs")){
			for(Song songToBeAdded: songsToBeAdded) {
				guifunctions.addSongToPlaylistGuiDB(songToBeAdded.getSongId(),textFieldAddSong.getText());
			}
			stage.close();
		}
		else if(textFieldAddSong.getText().equals("AllSongs")) {
			textFieldAddSong.setText(NO_ADDING_ALL_SONGS);
		}
		else {
			textFieldAddSong.setText(NO_SUCH_PLAYLIST);
		}
	}
	
	/**
	 * Quits the window.
	 */
	public void handleQuit() {
		stage.close();
	}
}
