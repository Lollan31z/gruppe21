package org.prog3.musicplayer2021.GUI.application;

import java.io.IOException;
import org.prog3.musicplayer2021.Exceptions.MP3PlayerException;
import org.prog3.musicplayer2021.Functions.GUIFunctions;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 * WindowAddPlaylistFXML handles adding playlist to the mp3-player.
 * @author pascal ney, fabian weinland, daniel gliemmo
 */
public class WindowAddPlaylistFXML {
	private static final String PLAYLIST_ALREADY_THERE = "This playlist already exists!";
	private static final String FXML_PATH = "WindowAddPlaylist.fxml";
	private static final String CSS_PATH = "/org/prog3/musicplayer2021/GUI/application/MainstyleSheet.css";
	private static final String STAGE_TITLE = "Please enter a playlist-name";
	
	@FXML
	private TextField textFieldAddPlaylist = new TextField();
	private GUIFunctions guifunctions = new GUIFunctions();
	private static Stage stage;
	private static Scene sceneAddPlaylist;
	
	/**
	 * Launches the window for adding playlists
	 * @throws IOException
	 * @throws MP3PlayerException
	 */
	public static void launch() throws IOException, MP3PlayerException {
		FXMLLoader fxmlLoader = new FXMLLoader(WindowAddPlaylistFXML.class.getResource(FXML_PATH));
	    Parent root = (Parent) fxmlLoader.load();
	    stage = new Stage();
	    stage.initModality(Modality.APPLICATION_MODAL);
	    stage.initStyle(StageStyle.UTILITY);
	    //stageAttr.initStyle(StageStyle.UNDECORATED);
	    stage.setTitle(STAGE_TITLE);
	    sceneAddPlaylist = new Scene(root, 550, 300);
	    sceneAddPlaylist.getStylesheets().add(CSS_PATH);
	    stage.setScene(sceneAddPlaylist);
	    stage.setMaxHeight(335);
	    stage.setMaxWidth(570);
	    stage.setMinHeight(335);
	    stage.setMinWidth(570);
	    stage.show();
	}
	
	
	/**
	 * Adds playlists to the mp3-player.
	 * @throws MP3PlayerException
	 */
	public void handleAddPlaylist() throws MP3PlayerException {
		String playlistName = textFieldAddPlaylist.getText();
		if (guifunctions.findPlaylistByNameGuiDB(playlistName) || playlistName.equals(PLAYLIST_ALREADY_THERE)){
			textFieldAddPlaylist.setText(PLAYLIST_ALREADY_THERE);
		} else {
			guifunctions.addPlaylistGuiDB(playlistName);
			stage.close();
		}
	}
	
	
	/**
	 * quits the window.
	 */
	public void handleQuit() {
		stage.close();
	}
}
