package org.prog3.musicplayer2021.GUI.application;


import javafx.stage.*;
import java.util.List;
import org.prog3.musicplayer2021.Functions.GUIFunctions;
import org.prog3.musicplayer2021.Functions.Song;
import org.prog3.musicplayer2021.GUI.languageSettings.ButtonClass;
import org.prog3.musicplayer2021.GUI.languageSettings.TitelClass;
import javafx.scene.*;
import javafx.scene.layout.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;

	
/**
 * Handles removing music from the 'AllSongs'-Playlist
 * @author pascal ney, fabian weinland, daniel gliemmo
 *
 */
public class WindowRemoveMusicAllSongs {
		private static Stage windowRemoveMusic;
		private static Scene sceneRemoveMusic;
		private static Button btnQuit, remove;
		private static ButtonClass button2;
		private static VBox layoutRemoveMusic;
		private static final String BACKGROUND_COLOR = "-fx-background-color: #5c5c5c;";
		private static final String STYLE_SELECTED = "-fx-font-weight: bold; "
				+ "-fx-background-color: #2e2e2e; -fx-border-color: cyan; -fx-text-fill: cyan;";
		private static final String TEXT_STYLE = "-fx-font-weight: bold; -fx-text-fill:cyan;";
		private static final String BTN_STYLE = "-fx-font-weight: bold; -fx-background-color: #2e2e2e; -fx-text-fill: cyan;";
		private static final String REMOVE_SONG_FROM_PLAYLIST = "Remove song from playlist";
		private static final String ARE_YOU_SURE = "Are you sure about that?";
		private static final String REMOVE_BUTTON_PROMPT = "Remove";
		private static final String QUIT_BUTTON_PROMPT = "Quit";
		private static final String BUTTON_FILL = "#4a4a4a";
		private static final String TEXT_PROMPT = "Deleting from 'AllSongs' will remove the song from your system as well!";
		private static final String CSS_PATH = "MainstyleSheet.css";
		private static CheckBox checkBoxAreYouSure;
		private static TitelClass title;
		private static TextField textfield2;
		private static Text areYouSureText;
		private static GUIFunctions guifunctions = new GUIFunctions();
		private static Label label;
		
		/**
		 * Launches the remove-music-window for 'AllSongs'
		 * @param songs - list of chosen songs
		 */
		public static void launch(List<Song> songs) {
			checkBoxAreYouSure = new CheckBox(ARE_YOU_SURE);
			title = new TitelClass();
			button2 = new ButtonClass();
			windowRemoveMusic = new Stage();
			windowRemoveMusic.initModality(Modality.APPLICATION_MODAL);
			windowRemoveMusic.initStyle(StageStyle.UTILITY);
			windowRemoveMusic.setTitle(REMOVE_SONG_FROM_PLAYLIST);
			areYouSureText = new Text(TEXT_PROMPT);
			areYouSureText.setFill(Color.CYAN);
			areYouSureText.setStyle(TEXT_STYLE);
			layoutRemoveMusic = new VBox(8);
			layoutRemoveMusic.setStyle(BACKGROUND_COLOR); 
			label = new Label();
			label.setText(ARE_YOU_SURE);
			label.setTextFill(Color.WHITE);			
			remove = new Button(REMOVE_BUTTON_PROMPT);
			remove.setStyle(BTN_STYLE);
			remove.setDisable(true);
			remove.setBackground(new Background(new BackgroundFill(Color.valueOf(BUTTON_FILL), null, null)));
			remove.setOnMousePressed(setPressedStyle(remove));
			remove.setOnMouseReleased(setReleasedStyle(remove));
			remove.setOnAction(onActionRemove(songs));
			checkBoxAreYouSure.setOnAction(enableDisableRemoveButton(checkBoxAreYouSure, remove));
			checkBoxAreYouSure.setTextFill(Color.CYAN);
			checkBoxAreYouSure.setStyle(BTN_STYLE);
			checkBoxAreYouSure.setPrefHeight(26);
			
			
			
			btnQuit = new Button(QUIT_BUTTON_PROMPT);
		    btnQuit.setBackground(new Background(new BackgroundFill(Color.valueOf(BUTTON_FILL), null, null)));
		    btnQuit.setStyle(BTN_STYLE);
		    btnQuit.setOnMousePressed(setPressedStyle(btnQuit));
		    btnQuit.setOnMouseReleased(setReleasedStyle(btnQuit));
		    btnQuit.setOnAction(e -> windowRemoveMusic.close());
			
			sceneRemoveMusic = new Scene(layoutRemoveMusic,490,190);
			sceneRemoveMusic.getStylesheets().add(WindowRemoveMusicAllSongs.class.getResource(CSS_PATH).toExternalForm());
			layoutRemoveMusic.getChildren().addAll(areYouSureText,checkBoxAreYouSure,remove,btnQuit);
			layoutRemoveMusic.setAlignment(Pos.CENTER);
			
			
			windowRemoveMusic.setScene(sceneRemoveMusic);
			windowRemoveMusic.setMaxHeight(200);
			windowRemoveMusic.setMinHeight(200);
			windowRemoveMusic.setMaxWidth(500);
			windowRemoveMusic.setMinWidth(500);
			windowRemoveMusic.showAndWait(); 
		
	}
		
		/**
		 * Removes songs from the database and the file-system.
		 * @param songs
		 * @return
		 */
		public static EventHandler<ActionEvent> onActionRemove(List<Song> songs){
			return new EventHandler<ActionEvent>() {
				@Override
				public void handle(ActionEvent event) {
					try {
						for(Song song: songs) {
							guifunctions.removeSongFromPlaylistGuiDB("AllSongs",song.getSongId());
						}
						windowRemoveMusic.close();				
					} catch (Exception e1) {
					     	e1.printStackTrace();
					}
				}
			};
		}
		
		/**
		 * handles disabling and enabling the remove button on checking the checkbox.
		 * @param checkBox
		 * @param btnRemove
		 * @return
		 */
		public static EventHandler<ActionEvent> enableDisableRemoveButton(CheckBox checkBox, Button btnRemove){
			return new EventHandler<ActionEvent>() {
				@Override
				public void handle(ActionEvent event) {
					if(checkBox.isSelected()) {
						checkBox.setStyle(STYLE_SELECTED);
						btnRemove.setDisable(false);
					}
					else {
						checkBox.setStyle(BTN_STYLE);
						btnRemove.setDisable(true);
					}
			}
			};
		}
		
		/**
		 * Sets style, whe button is pressed
		 * @param buttonPressed
		 * @return
		 */
		public static EventHandler<MouseEvent> setPressedStyle(Button buttonPressed){
			return new EventHandler<MouseEvent>() {
				@Override
				public void handle(MouseEvent event) {
						buttonPressed.setStyle(STYLE_SELECTED);	
				}
			};
			
		}
		
		/**
		 * Sets style, when button is pressed
		 * @param buttonPressed
		 * @return
		 */
		public static EventHandler<MouseEvent> setReleasedStyle(Button buttonReleased){
			return new EventHandler<MouseEvent>() {
				@Override
				public void handle(MouseEvent event) {
					buttonReleased.setStyle(BTN_STYLE);
				}
			};
			
		}
		
		
		
}
