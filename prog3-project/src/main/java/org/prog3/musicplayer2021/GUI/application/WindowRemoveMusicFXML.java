package org.prog3.musicplayer2021.GUI.application;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import org.prog3.musicplayer2021.Exceptions.MP3PlayerException;
import org.prog3.musicplayer2021.Functions.GUIFunctions;
import org.prog3.musicplayer2021.Functions.Playlist;
import org.prog3.musicplayer2021.Functions.Song;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 * WindowRemoveMusic handles removing Music from playlists
 * @author pascal ney, fabian weinland, daniel gliemmo
 *
 */
public class WindowRemoveMusicFXML {
	private static final String FXML_PATH = "WindowRemoveSong.fxml";
	private static final String CSS_PATH = "/org/prog3/musicplayer2021/GUI/application/MainStylesheet.css";
	@FXML
	private static CheckBox areYouSureCheckBox = new CheckBox();
	@FXML
	private static Button removeSongBtn;
	@FXML
	private Text textCannotBeDeleted = new Text();
	private static Stage windowRemoveSong;
	private static List<Song> songsChosen;
	private static Scene sceneRemoveSong;
	private static GUIFunctions guifunctions = new GUIFunctions();
	private static Playlist current;
	
	/**
	 * Launches the window to add music to playlists and sets currentPlaylist and the songs to be added.
	 * @param songs
	 * @param currentPlaylist
	 * @throws IOException
	 */
    public static void launch(List<Song> songs, Playlist currentPlaylist) throws IOException {
    	removeSongBtn = new Button();
		songsChosen = new LinkedList<Song>();
		songsChosen.addAll(songs);
		current = currentPlaylist;
		windowRemoveSong = new Stage();
		FXMLLoader fxmlLoader = new FXMLLoader(WindowRemoveMusicFXML.class.getResource(FXML_PATH));
		Parent root = (Parent) fxmlLoader.load();
		windowRemoveSong.initModality(Modality.APPLICATION_MODAL);
		windowRemoveSong.initStyle(StageStyle.UTILITY);
		windowRemoveSong.setTitle("Remove a Song");
		sceneRemoveSong = new Scene(root, 550, 300);
		sceneRemoveSong.getStylesheets().add(CSS_PATH);
		windowRemoveSong.setScene(sceneRemoveSong);
		windowRemoveSong.setScene(sceneRemoveSong);
	    windowRemoveSong.setMaxHeight(335);
	    windowRemoveSong.setMaxWidth(570);
	    windowRemoveSong.setMinHeight(335);
	    windowRemoveSong.setMinWidth(570);
	    removeSongBtn.setDisable(true);
	    removeSongBtn.disabledProperty();
	    windowRemoveSong.show();
	}
	
	/**
	 * removes a song from a playlist.
	 * @throws IOException 
	*/
	
	public void handleRemoveSong() throws MP3PlayerException, IOException {
		for(Song songChosen: songsChosen) {
			guifunctions.removeSongFromPlaylistGuiDB(current.getPlaylistName(), songChosen.getSongId());
		}
		windowRemoveSong.close();
	}
	
	/**
	 * Quits the window.
	 */
	public void handleQuit() {
		windowRemoveSong.close();
	}
}

