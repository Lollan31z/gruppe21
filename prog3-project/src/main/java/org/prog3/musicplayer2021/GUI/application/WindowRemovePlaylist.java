package org.prog3.musicplayer2021.GUI.application;

import org.prog3.musicplayer2021.Functions.GUIFunctions;
import org.prog3.musicplayer2021.Functions.Playlist;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
	
/**
 * the class WindowRemovePlaylist opens a window which add music
 * 
 * @author fabian, daniel
 *
 *
 **/
 
	
public class WindowRemovePlaylist {

	private static final String BUTTON_FILL = "#2e2e2e";
	private static final String TEXT_STYLE = "-fx-font-weight: bold;";
	private static final String REMOVE_PLAYLIST = "Remove playlist";
	private static final String BACKGROUND_COLOR = "-fx-background-color: #5c5c5c;";
	private static final String ARE_YOU_SURE = "Are you sure to remove current Playlist?";
	private static final String CLOSE_WINDOW = "Quit";
	private static Stage windowRemovePlaylist;
	private static Scene sceneRemovePlaylist;
	private static Button quitButton, removeButton;
	private static VBox layoutRemoveMusic;
	private static Label questionLabel;	
	private static GUIFunctions guifunctions = new GUIFunctions();
		
	/**
	 * Opens the removePlaylist-Window and sets the chosen Playlist.
	 * @param playlist
	 */
	public static void removePlaylist(Playlist playlist) {
			
		windowRemovePlaylist = new Stage();
		windowRemovePlaylist.initModality(Modality.APPLICATION_MODAL); //priority window
		windowRemovePlaylist.initStyle(StageStyle.UTILITY);
		windowRemovePlaylist.setTitle(REMOVE_PLAYLIST);
			
		layoutRemoveMusic = new VBox(8);
		layoutRemoveMusic.setStyle(BACKGROUND_COLOR);
			
		questionLabel = new Label();
		questionLabel.setText(ARE_YOU_SURE);
		questionLabel.setTextFill(Color.CYAN);
		questionLabel.setStyle(TEXT_STYLE);
			
			
		quitButton = new Button(CLOSE_WINDOW);
		quitButton.setBackground(new Background(new BackgroundFill(Color.valueOf(BUTTON_FILL), null, null)));
		quitButton.setTextFill(Color.CYAN);
		quitButton.setStyle(TEXT_STYLE);
		quitButton.setOnAction(e -> windowRemovePlaylist.close());
		    
			
		removeButton = new Button(REMOVE_PLAYLIST);
		removeButton.setBackground(new Background(new BackgroundFill(Color.valueOf(BUTTON_FILL), null, null)));
	    removeButton.setTextFill(Color.CYAN);
	    removeButton.setStyle(TEXT_STYLE);
		removeButton.setOnAction(e-> {
			try {
				guifunctions.removePlaylistGuiDB(playlist.getPlaylistName());
				windowRemovePlaylist.close();
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		});
			
			
		sceneRemovePlaylist = new Scene(layoutRemoveMusic,500,200);
		layoutRemoveMusic.getChildren().addAll(questionLabel,removeButton,quitButton);
		layoutRemoveMusic.setAlignment(Pos.CENTER);
			
			
		windowRemovePlaylist.setScene(sceneRemovePlaylist);
		windowRemovePlaylist.setMaxHeight(200);
		windowRemovePlaylist.setMinHeight(200);
		windowRemovePlaylist.setMaxWidth(500);
		windowRemovePlaylist.setMinWidth(500);
		windowRemovePlaylist.showAndWait(); 
		
	}
}

