package org.prog3.musicplayer2021.GUI.languageSettings;

import javafx.scene.control.Button;

/**
 * ButtonClass returns the different Buttons with the selected Language
 * @author fabian weinland, daniel gliemmo
 *
 */

public class ButtonClass {
	private static Button buttonAbbrechen;
	private static Button buttonJa;
	private static Button buttonNein;
	private ButtonLanguage languageButtons;
	
	/**
	 * ButtonClass creates the different button objects
	 */
	public ButtonClass() {
		languageButtons = new ButtonLanguage();
		buttonAbbrechen = new Button(languageButtons.getAbbrechen());
		buttonJa = new Button(languageButtons.getJa());
		buttonNein = new Button(languageButtons.getNein());
	}
	/**
	 * the method buttonAbbrechen returns the cancel button
	 * @return the cancel button
	 */
	public Button buttonAbbrechen() {
		return buttonAbbrechen;
	}
	/**
	 * the method buttonJa returns the yes button
	 * @return yes button
	 */
	public Button buttonJa() {
			return buttonJa;
	}
	/**
	 * method buttonNein 
	 * @return no button
	 */
	public Button buttonNein() {
		return buttonNein;
	}
	/**
	 * method buttonNew creates a button with variable fill
	 * @param msg is the text in the button
	 * @return the created button
	 */
	public Button buttonNew(String msg) {
		return new Button(msg);
	}
	/**
	 * buttonLanguage for the language button in the main window
	 * @return the String for the language button
	 */
	public String buttonLanguage() {
		return languageButtons.getLang();
	}
	public String buttonAddMusic() {
		return languageButtons.getMusic();
	}
	public String buttonNewPlaylist() {
		return languageButtons.getPlaylist();
	}
	public String menuFile() {
		return languageButtons.getFile();
	}
	public String menuEdit() {
		return languageButtons.getEdit();
	}
	public String menuHelp() {
		return languageButtons.getHelp();
	}
}
