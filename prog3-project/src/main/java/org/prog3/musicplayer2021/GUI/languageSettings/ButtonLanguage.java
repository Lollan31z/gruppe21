package org.prog3.musicplayer2021.GUI.languageSettings;

import org.prog3.musicplayer2021.GUI.application.MainWindowController;

/**
 * The class ButtonLanguage contains the different Strings for each button
 * @author fabian weinland, daniel gliemmo
 */
public class ButtonLanguage {
	private static final String deutsch = "Deutsch";
	private static final String english = "English";
	private static final String MSG_WRONG_LANGUAGE = "ERR wrong language!";
	private String abbrechen;
	private String ja;
	private String nein;
	private String lang;
	private String music;
	private String playlist;
	private String file;
	private String edit;
	private String help;
	
	
	public ButtonLanguage() {
		setButtons();
	}
	/**
	 * setButtons set the different buttons with the selected Language
	 */
	private void setButtons() {
		if(MainWindowController.languageString  == null) {
			MainWindowController.languageString = "English";
		}
		if(MainWindowController.languageString.equals(deutsch)) {
			this.abbrechen = "abbrechen";
			this.ja = "Ja";
			this.nein = "Nein";
			this.lang = "Sprache";
			this.music = "Musik hinzufügen";
			this.playlist = "Playlist hinzufügen";
			this.file = "Datei";
			this.edit = "Bearbeiten";
			this.help = "Hilfe";
		}else if(MainWindowController.languageString.equals(english)) {
			this.abbrechen = "cancel";
			this.ja = "Yes";
			this.nein = "no";
			this.lang = "Language";
			this.music = "Add Music";
			this.playlist = "New Playlist";
			this.file = "File";
			this.edit = "Edit";
			this.help = "Help";
			} 
		else throw new IllegalArgumentException(MSG_WRONG_LANGUAGE);
	}
	/**
	 * getJa for yes button
	 * @return the text for yes button
	 */
	public String getJa() {
		return ja;
	}
	/**
	 * getNein for no button
	 * @return txt for no button
	 */
	public String getNein() {
		return nein;
	}
	/**
	 * getAbbrechen for cancle button
	 * @return txt for cancle button
	 */
	public String getAbbrechen() {
		return abbrechen;
	}
	/**
	 * getLang for Language button
	 * @return txt for language button
	 */
	public String getLang() {
		setButtons();
		return lang;
	}
	public String getMusic() {
		return music;
	}
	public String getPlaylist() {
		return playlist;
	}
	public String getFile() {
		return file;
	}
	public String getEdit() {
		return edit;
	}
	public String getHelp() {
		return help;
	}
}
