package org.prog3.musicplayer2021.GUI.languageSettings;

import javafx.scene.control.Label;

/**
 * LabelClass for handle Language of labels
 * @author fabian weinland, daniel gliemmo
 */
public class LabelClass {
	private static Label labelSavePlaylist;
	
	private LabelLanguage labelLanguage;
	
	/**
	 * LabelClass Constructor creates the different label objects
	 */
	public LabelClass(){
		labelLanguage = new LabelLanguage();
		labelSavePlaylist = new Label(labelLanguage.getSaveLabel());
		
	}
	/**
	 * labelSavePlaylist handle save playlist window label
	 * @return return the Label in the save playlist window
	 */
	public Label labelSavePlaylist() {
		return labelSavePlaylist;
	}

}
