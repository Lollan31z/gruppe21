package org.prog3.musicplayer2021.GUI.languageSettings;

import org.prog3.musicplayer2021.GUI.application.MainWindowController;

/**
 * Class LabelLanguage extends the text for the labels
 * @author fabian weinland, daniel gliemmo
 *
 */
public class LabelLanguage {
	private static final String deutsch = "Deutsch";
	private static final String english = "English";
	private static final String playlistSpeichern = "Möchten Sie die aktuelle Playlist speichern?";
	private static final String playlistSave = "Do you want to save the current playlist?";
	private String saveLabel;
	private static final String MSG_WRONG_LANGUAGE = "ERR wrong language!";
	
	public LabelLanguage(){
		setLabels();
	}
	/**
	 * setLabels select the correct text for the labels
	 */
	private void setLabels() {
		if(MainWindowController.languageString  == null) {
			MainWindowController.languageString = "English";
		}
		if(MainWindowController.languageString.equals(deutsch)) {
			saveLabel = playlistSpeichern;
		} else if(MainWindowController.languageString.equals(english)) {
			saveLabel = playlistSave;
		}
		else throw new IllegalArgumentException(MSG_WRONG_LANGUAGE);
	}
	/**
	 * getSaveLabel 
	 * @return text for saveLabel
	 */
	public String getSaveLabel() {
		return saveLabel;
	}

}
