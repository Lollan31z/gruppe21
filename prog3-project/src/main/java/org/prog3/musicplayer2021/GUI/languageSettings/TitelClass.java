package org.prog3.musicplayer2021.GUI.languageSettings;
/**
 * TitleClass handle the titles 
 * select for each title the text in the correct language
 * @author fabian weinland, daniel gliemmo
 *
 */
public class TitelClass {
	private TitleLanguage titleLanguage;
	private String change;
	private String add;
	private String close;
	
	/**
	 * TitleCLass Constructor creates set the diffrent titles
	 */
	public TitelClass() {
		titleLanguage = new TitleLanguage();
		change = titleLanguage.getChangeLanguage();
		add = titleLanguage.getAddLanguage();
		close = titleLanguage.getCloseLanguage();
		
	}
	/**
	 * titleChangeLanguage for the title of the language window
	 * @return the title for the language window
	 */
	public String titleChangeLanguage() {
		return change;
	}
	/**
	 * getAdd 
	 * @return title of Add Music Window
	 */
	public String getAdd() {
		return add;
	}
	/**
	 * getClose 
	 * @return title of close Window
	 */
	public String getClose() {
		return close;
	}
}
