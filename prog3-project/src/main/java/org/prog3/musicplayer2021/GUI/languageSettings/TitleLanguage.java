package org.prog3.musicplayer2021.GUI.languageSettings;

import org.prog3.musicplayer2021.GUI.application.MainWindowController;

/**
 * TitleLanguage set the titles with correct language
 * @author fabian weinland, daniel gliemmo
 *
 */

public class TitleLanguage {

	private static final String deutsch = "Deutsch";
	private static final String english = "English";
	private String change;
	private String add;
	private String close;
	
	private static final String MSG_WRONG_LANGUAGE = "ERR wrong language!";
	
	public TitleLanguage() {
		setTitles();
	}
	/**
	 * setTitles decides the correct Language and sets the titles
	 */
	private void setTitles() {
			if(MainWindowController.languageString  == null) {
				MainWindowController.languageString = "Deutsch";
			}
			if(MainWindowController.languageString.equals(deutsch)) {
				this.change = "Sprache ändern";
				this.add = "Musik hinzufügen";
				this.close = "Fenster schließen";
			}else if(MainWindowController.languageString.equals(english)) {
				this.change = "Change language";
				this.add ="Add Music";
				this.close = "Close Window";
				} 
			else throw new IllegalArgumentException(MSG_WRONG_LANGUAGE);
	}
	/**
	 * getChangeLanguage
	 * @return title of Language Window
	 */
	public String getChangeLanguage() {
		return this.change;
	}
	/**
	 * getAddLanguage 
	 * @return title of add music window
	 */
	public String getAddLanguage() {
		return add;
	}
	/**
	 * getCloseLanguage
	 * @return title of close window
	 */
	public String getCloseLanguage() {
		return close;
	}

}
