package org.prog3.musicplayer2021.GUI.languageSettings;

import org.prog3.musicplayer2021.GUI.application.MainWindowController;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.geometry.Pos;
import javafx.scene.control.*;

/**
 * WindowChangeLanguage creates a Window with buttons. Each button select 
 * one Language and change the language of windows
 * @author fabian weinland
 *
 */
public class WindowChangeLanguage {

	private static Stage windowLanguage;
	private static Scene sceneLanguage;
	private static VBox layoutLanguage;
	private static Button deutsch;
	private static Button english;
	private static TitelClass title;
	
	 
	private WindowChangeLanguage() {
	}
	/**
	 * changeLanguage handles the click on a Language Button
	 * and sets the global language
	 */
	public static void changeLanguage() {
		title = new TitelClass();
		
		
		deutsch = new Button("Deutsch");
		deutsch.setBackground(new Background(new BackgroundFill(Color.valueOf("#4a4a4a"), null, null)));
		deutsch.setTextFill(Color.WHITE);
		deutsch.setOnAction(e -> {MainWindowController.languageString = "Deutsch";
									windowLanguage.close();
		});
		
		
		english = new Button("English");
		english.setBackground(new Background(new BackgroundFill(Color.valueOf("#4a4a4a"), null, null)));
		english.setTextFill(Color.WHITE);
		english.setOnAction(e -> {MainWindowController.languageString ="English";
									windowLanguage.close();
		});
		
		windowLanguage = new Stage();
		windowLanguage.initModality(Modality.APPLICATION_MODAL); //priority window
		windowLanguage.setTitle(title.titleChangeLanguage());
		
		
		
		layoutLanguage = new VBox(20);
		layoutLanguage.setStyle("-fx-background-color: #5c5c5c;");
		layoutLanguage.getChildren().addAll(deutsch,english);
		layoutLanguage.setAlignment(Pos.CENTER);
		
		
		sceneLanguage = new Scene(layoutLanguage,400,200);
		windowLanguage.setScene(sceneLanguage);
		windowLanguage.showAndWait();
		
	}
	
	
}

