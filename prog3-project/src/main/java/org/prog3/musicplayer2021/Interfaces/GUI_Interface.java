package org.prog3.musicplayer2021.Interfaces;

import java.io.IOException;
import java.util.List;

import org.prog3.musicplayer2021.Exceptions.InitiateFileException;
import org.prog3.musicplayer2021.Exceptions.MP3PlayerException;
import org.prog3.musicplayer2021.Functions.Playlist;
import org.prog3.musicplayer2021.Functions.Song;
/**
 * This interface contains functions for the GUI to use.
 * @author pascal n
 *
 */
public interface GUI_Interface {
	
	/**
	 * Initiates the filepath to the songs.
	 * @param path
	 * @throws IOException
	 */
	void initiateFileToSongs(String path) throws IOException;
	
	/**
	 * Returns the path to the songs
	 * @return Path to the songs
	 * @throws IOException
	 * @throws MP3PlayerException
	 */
	String getDirPathToSongs() throws IOException, MP3PlayerException;
	
	/**
	 * checks if the path to the songs exists
	 * @return true if yes, false if no
	 * @throws IOException
	 * @throws MP3PlayerException
	 */
	boolean dirPathExists() throws IOException, MP3PlayerException;
	
	/**
	 * Finds a songs in the database by their primary key.
	 * @param primaryKey
	 * @return the song
	 */
	Song findSongInDatabaseByPrimaryKey(final int primaryKey);
	
	/**
	 * Finds songs in the database by title.
	 * @param title
	 * @return the song
	 */
	List<Song> findSongInDatabaseByTitle(final String title);
	
	/**
	 * Finds a song in the database by its path.
	 * @param path
	 * @return the song
	 */
	Song findSongInDatabaseByPath(final String path);
	
	/**
	 * Finds songs in the database by their artist.
	 * @param artist
	 * @return the song
	 */
	List<Song> findSongsInDatabaseByArtist(final String artist);
	
	/**
	 * Finds songs in the database by their albumname.
	 * @param albumName
	 * @return the song
	 */
	List<Song> findSongsInDatabaseByAlbumName(final String albumName);
	
	/**
	 * Finds all songs in the database as a list.
	 * @return the list of all songs
	 */
	List<Song> findAllSongsInDatabaseAsList();
	
	/**
	 * Counts all songs in the database.
	 * @return the song-count
	 */
	int countAllSongsInDatabase();
	
	/**
	 * Counts all playlists in the database.
	 * @return the playlist-count
	 */
	int countAllPlaylistsInDatabase();
	
	/**
	 * Returns all songs in the database as a playlist
	 * @return the playlist with all songs
	 * @throws MP3PlayerException
	 */
	Playlist findAllSongsInDatabaseAsPlaylist() throws MP3PlayerException;
	
	/**
	 * deletes a song from the database
	 * @param songId
	 * @throws MP3PlayerException
	 * @throws IOException
	 */
	void deleteSongFromDatabase(final int songId) throws MP3PlayerException, IOException;
	
	/**
	 * Returns songs in a playlist as a List
	 * @param temp
	 * @return
	 */
	List<Song> showSongsInPlaylist(Playlist temp);
	
	/**
	 * Returns all Playlists in a List.
	 * @return
	 */
	List<Playlist> showAllPlaylists();
	
	/**
	 * Adds a song to the playlist.
	 * @param songId
	 * @param playlistname
	 * @throws MP3PlayerException
	 */
	void addSongToPlaylistGuiDB(int songId, String playlistname) throws MP3PlayerException;

	/**
	 * checks if playlist name exists in database
	 * @return Playlist
	 */
	boolean findPlaylistByNameGuiDB(String playlistname) throws MP3PlayerException;

	/**
	 * add new Playlist to DB
	 */
	void addPlaylistGuiDB(String playlistname) throws MP3PlayerException;

	/**
	 * remove Playlist from DB
	 */
	void removePlaylistGuiDB(String playlistname) throws MP3PlayerException;

	/**
	 * checks if playlist name exists in database
	 * @return Playlist
	 * @throws IOException 
	 */
	void removeSongFromPlaylistGuiDB(String playlistname, int songId) throws MP3PlayerException, IOException;

	/**
	 * checks if db has song with name tosearch
	 * @param tosearch
	 * @return List<Song>
	 */
	List<Song> searchName(String tosearch);

	/**
	 * checks if db has song with Artist tosearch
	 * @param tosearch
	 * @return List<Song>
	 */
	List<Song> searchArtist(String tosearch);

	/**
	 * checks if db has song with Album tosearch
	 * @param tosearch
	 * @return List<Song>
	 */
	List<Song> searchAlbum(String tosearch);

	/**
	 * Sorts the playlist by the given sortStatus
	 * @param playlist
	 * @param sortStatus
	 */
	void sortPlaylistInDatabase(Playlist playlist, int sortStatus);

	/**
	 * Changes the title of the given song
	 * @param title
	 * @param songId
	 * @throws MP3PlayerException
	 */
	void setSongTitle(String title, int songId) throws MP3PlayerException;

	/**
	 * Changes the album of the given song
	 * @param album
	 * @param songId
	 * @throws MP3PlayerException
	 */
	void setSongAlbum(String album, int songId) throws MP3PlayerException;

	/**
	 * Changes the artist of the given song
	 * @param artist
	 * @param songId
	 * @throws MP3PlayerException
	 */
	void setSongArtist(String artist, int songId) throws MP3PlayerException;
}
