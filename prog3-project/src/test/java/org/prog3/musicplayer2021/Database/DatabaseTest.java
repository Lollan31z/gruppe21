package org.prog3.musicplayer2021.Database;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import org.dbunit.*;
import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.operation.DatabaseOperation;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.prog3.musicplayer2021.Exceptions.*;
import org.prog3.musicplayer2021.Functions.*;


/**
 * TestClass for database
 * @author brian mueller, pascal vinzent
 *
 */
public class DatabaseTest  {

public IDatabaseConnection getDatabaseConnection() throws SQLException, DatabaseUnitException,
          ClassNotFoundException {

	Connection jdbcConnection = DriverManager.getConnection("jdbc:sqlite:musicplayer.db");
	//IDatabaseConnection connection = new DatabaseConnection(jdbcConnection);
	//DatabaseConfig dbConfig = connection.getConfig();
	//dbConfig.setProperty(DatabaseConfig.PROPERTY_RESULTSET_TABLE_FACTORY, new ForwardOnlyResultSetTableFactory());
	return new DatabaseConnection(jdbcConnection);
}


public void fullDatabaseExport(IDatabaseConnection connection) throws 
        DataSetException, SQLException, FileNotFoundException, IOException{
	IDataSet fullDataSet = connection.createDataSet();
	FlatXmlDataSet.write(fullDataSet, new
		FileOutputStream("actualdataset.xml"));
}


private void fullDatabaseImport(File file) throws
        ClassNotFoundException, DatabaseUnitException,
        IOException,SQLException{
	IDatabaseConnection connection = getDatabaseConnection();
	FlatXmlDataSetBuilder builder = new FlatXmlDataSetBuilder();
	IDataSet dataSet = builder.build(file);
	
	DatabaseOperation.CLEAN_INSERT.execute(connection, dataSet);
}


@BeforeEach
public void setUp() throws
ClassNotFoundException, DatabaseUnitException,
IOException,SQLException {
	File xmlFile = new File("actualdataset.xml");
	boolean xmlFileExists = xmlFile.exists();
	assertTrue(xmlFileExists);
	fullDatabaseImport(xmlFile);
}


@Test //Trouble took place here
public void testAddSong() throws Exception  {
	File expectedXmlFile = new File("expecteddatasetAddSongTest.xml");
	FlatXmlDataSetBuilder builder = new FlatXmlDataSetBuilder();
	
	IDataSet expectedDataSet = builder.build(expectedXmlFile);
	ITable expectedTable = expectedDataSet.getTable("Songs");
	
	Song tobeadded = new Song("toBeAdded", "makko", "321.mp3", "Switch Heels");
	Database.addSong(tobeadded);
	
	IDatabaseConnection connection = getDatabaseConnection();
	IDataSet actualDataSet = connection.createDataSet();
	ITable actualTable = actualDataSet.getTable("Songs");
	
	Assertion.assertEquals(expectedTable, actualTable);
}


@Test 
public void testFindSongByPrimaryKey() throws Exception {
	Song found =Database.findSongByPrimaryKey(1);
	assertEquals(found.getAlbum(), "Water");
	assertEquals(found.getArtist(), "Joe");
	assertEquals(found.getTitle(), "Waterfall");
	assertEquals(found.getPath(), "1234.mp3");
	assertEquals(found.getSongId(),1);
}


@Test 
public void testFindSongByTitle() throws Exception {
    List<Song> found = (List<Song>)Database.findSongByTitle("Waterfall");
		assertEquals(found.get(0).getAlbum(),"Water");
		assertEquals(found.get(0).getArtist(),"Joe");
		assertEquals(found.get(0).getTitle(),"Waterfall");
		assertEquals(found.get(0).getPath(),"1234.mp3");
}


@Test 
public void testFindSongByPath() throws Exception {
	Song found =(Song)Database.findSongByPath("1234.mp3");
	assertEquals(found.getAlbum(), "Water");
	assertEquals(found.getArtist(), "Joe");
	assertEquals(found.getTitle(), "Waterfall");
	assertEquals(found.getPath(), "1234.mp3");
	assertEquals(found.getSongId(),1);
}


@Test 
public void testFindSongByArtist() throws SongException, IOException {
	
	ArrayList<Song> found = (ArrayList<Song>) Database.findSongsByArtist("Joe");
	Song eins = new Song("Waterfall", "Joe", "1234.mp3", "Water");
	Song zwei = new Song("Eis", "Joe", "1235.mp3", "Water");
	Song drei = new Song("Erdbeben", "Joe", "455.mp3", "Water");
    List<Song> shouldBe = new ArrayList<Song>();
    shouldBe.add(eins);
    shouldBe.add(zwei);
    shouldBe.add(drei);
    for(int i = 0; i<found.size();i++) {
    	assertEquals(shouldBe.get(i).getAlbum(), found.get(i).getAlbum());
    	assertEquals(shouldBe.get(i).getArtist(), found.get(i).getArtist());
    	assertEquals(shouldBe.get(i).getPath(), found.get(i).getPath());
    	assertEquals(shouldBe.get(i).getTitle(), found.get(i).getTitle());
    }
}


@Test 
public void testFindSongByAlbumName() throws SongException, IOException {;	
	ArrayList<Song> found = (ArrayList<Song>) Database.findSongsByAlbumName("Joe");
	Song eins = new Song("Waterfall", "Joe", "1234.mp3", "Water");
	Song zwei = new Song("Eis", "Joe", "1235.mp3", "Water");
	Song drei = new Song("Erdbeben", "Joe", "455.mp3", "Water");
    eins.setSongId(1);
    zwei.setSongId(3);
    drei.setSongId(4);
    List<Song> shouldBe = new ArrayList<Song>();
    shouldBe.add(eins);
    shouldBe.add(zwei);
    shouldBe.add(drei);
    for(int i = 0; i<found.size();i++) {
    	assertEquals(shouldBe.get(i).getAlbum(), found.get(i).getAlbum());
    	assertEquals(shouldBe.get(i).getArtist(), found.get(i).getArtist());
    	assertEquals(shouldBe.get(i).getPath(), found.get(i).getPath());
    	assertEquals(shouldBe.get(i).getTitle(), found.get(i).getTitle());
    }
}


@Test 
public void testFindAllSongs() throws SongException, IOException {
	
	ArrayList<Song> found = (ArrayList<Song>) Database.findAllSongs();
	Song eins = new Song("Waterfall", "Joe", "1234.mp3", "Water");
	Song zwei = new Song("toBeRemoved", "Edo", "444.mp3", "Remove");
	Song drei = new Song("Eis", "Joe", "1235.mp3", "Water");
	Song vier = new Song("Erdbeben", "Joe", "455.mp3", "Water");
	Song fuenf = new Song("aaa", "aaa", "2234.mp3", "aaa");
	Song sechs = new Song("bbb", "bbb", "2235.mp3", "bbb");
	Song sieben = new Song("ccc", "ccc", "2236.mp3", "ccc");
	Song acht = new Song("ddd", "ddd", "2237.mp3", "ddd");
    List<Song> shouldBe = new ArrayList<Song>();
    shouldBe.add(eins);
    shouldBe.add(zwei);
    shouldBe.add(drei);
    shouldBe.add(vier);
    shouldBe.add(fuenf);
    shouldBe.add(sechs);
    shouldBe.add(sieben);
    shouldBe.add(acht);
    for(int i = 0; i<found.size();i++) {
    	assertEquals(shouldBe.get(i).getAlbum(), found.get(i).getAlbum());
    	assertEquals(shouldBe.get(i).getArtist(), found.get(i).getArtist());
    	assertEquals(shouldBe.get(i).getPath(), found.get(i).getPath());
    	assertEquals(shouldBe.get(i).getTitle(), found.get(i).getTitle());
    }
}


@Test 
public void testFindAllSongsAsPlaylist() throws Exception {
	
	Playlist found = Database.findAllSongsAsPlaylist();
	Song eins = new Song("Waterfall", "Joe", "1234.mp3", "Water");
	Song zwei = new Song("toBeRemoved", "Edo", "444.mp3", "Remove");
	Song drei = new Song("Eis", "Joe", "1235.mp3", "Water");
	Song vier = new Song("Erdbeben", "Joe", "455.mp3", "Water");
	Song fuenf = new Song("aaa", "aaa", "2234.mp3", "aaa");
	Song sechs = new Song("bbb", "bbb", "2235.mp3", "bbb");
	Song sieben = new Song("ccc", "ccc", "2236.mp3", "ccc");
	Song acht = new Song("ddd", "ddd", "2237.mp3", "ddd");
    
    Playlist shouldBe = new Playlist("AllSongs");
    shouldBe.addSongToPlaylist(eins);
    shouldBe.addSongToPlaylist(zwei);
    shouldBe.addSongToPlaylist(drei);
    shouldBe.addSongToPlaylist(vier);
    shouldBe.addSongToPlaylist(fuenf);
    shouldBe.addSongToPlaylist(sechs);
    shouldBe.addSongToPlaylist(sieben);
    shouldBe.addSongToPlaylist(acht);
    for(int i = 0; i<found.getSonglist().size();i++) {
    	assertEquals(shouldBe.getSonglist().get(i).getAlbum(), found.getSonglist().get(i).getAlbum());
    	assertEquals(shouldBe.getSonglist().get(i).getArtist(), found.getSonglist().get(i).getArtist());
    	assertEquals(shouldBe.getSonglist().get(i).getPath(), found.getSonglist().get(i).getPath());
    	assertEquals(shouldBe.getSonglist().get(i).getTitle(), found.getSonglist().get(i).getTitle());
    }
}


@Test
public void testRemoveSong() throws
ClassNotFoundException, DatabaseUnitException,
IOException,SQLException  {
	File expectedXmlFile = new File("expecteddatasetRemoveSongTest.xml");
	FlatXmlDataSetBuilder builder = new FlatXmlDataSetBuilder();
	IDataSet expectedDataSet = builder.build(expectedXmlFile);
	ITable expectedTable = expectedDataSet.getTable("Songs");
	
	Database.deleteSong(2);
	IDatabaseConnection connection = getDatabaseConnection();
	IDataSet actualDataSet = connection.createDataSet();
	ITable actualTable = actualDataSet.getTable("Songs");
	
	Assertion.assertEquals(expectedTable, actualTable);
}


@Test //Trouble took place here
public void testAddPlaylist() throws
ClassNotFoundException, DatabaseUnitException,
IOException,SQLException, PlaylistException  {
	File expectedXmlFile = new File("expecteddatasetAddPlaylistTest.xml");
	FlatXmlDataSetBuilder builder = new FlatXmlDataSetBuilder();
	IDatabaseConnection connection = getDatabaseConnection();
	IDataSet actualDataSet = connection.createDataSet();
	ITable actualTable = actualDataSet.getTable("Playlists");
	
	Playlist klassik = new Playlist("Klassik");
	Database.addPlaylist(klassik);
	
	IDataSet expectedDataSet = builder.build(expectedXmlFile);
	ITable expectedTable = expectedDataSet.getTable("Playlists");
	
	Assertion.assertEquals(expectedTable, actualTable);
}


@Test
public void testFindPlaylistByPrimaryKey() throws Exception  {
	
	Playlist found = Database.findPlaylistByPrimaryKey(1);
	assertEquals(found.getPlaylistId(),1);
	assertEquals(found.getPlaylistName(),"Rock");
}


@Test
public void testFindPlaylistByName() throws PlaylistException, IOException  {
	Playlist found = Database.findPlaylistByName("Rock");
	assertEquals(found.getPlaylistId(),1);
	assertEquals(found.getPlaylistName(),"Rock");
}


@Test
public void testAddSongToPlaylist() throws Exception {
	File expectedXmlFile = new File("expecteddatasetAddSongToPlaylistTest.xml");
	FlatXmlDataSetBuilder builder = new FlatXmlDataSetBuilder();
	IDataSet expectedDataSet = builder.build(expectedXmlFile);
	ITable expectedTable = expectedDataSet.getTable("Playlists");
	
	Database.addSongToPlaylist(Database.findSongByPath("1235.mp3").getSongId(),Database.findPlaylistByName("Rock").getPlaylistId());
	
	IDatabaseConnection connection = getDatabaseConnection();
	IDataSet actualDataSet = connection.createDataSet();
	ITable actualTable = actualDataSet.getTable("Playlists");
	
	Assertion.assertEquals(expectedTable, actualTable);
	
}

@Test //Trouble took place here
public void testshowSongsInPlaylist() throws Exception {
	File expectedXmlFile = new File("expectedShotSongsInPlaylist.xml");
	FlatXmlDataSetBuilder builder = new FlatXmlDataSetBuilder();
	IDataSet expectedDataSet = builder.build(expectedXmlFile);
	ITable expectedTable = expectedDataSet.getTable("Playlists");
	
	List<Song> found = Database.showSongsInPlaylist(1);
    Database.addSongToPlaylist(1, 6);
	Database.addSongToPlaylist(1,7);
	Database.addSongToPlaylist(1, 3);
	
	IDatabaseConnection connection = getDatabaseConnection();
	IDataSet actualDataSet = connection.createDataSet();
	ITable actualTable = actualDataSet.getTable("Playlists");
			
	Assertion.assertEquals(expectedTable, actualTable);
	
	Song eins = new Song("aaa", "aaa", "2234.mp3", "aaa");
	Song zwei = new Song("bbb", "bbb", "2235.mp3", "bbb");
	Song drei = new Song("Eis", "Joe", "1235.mp3", "Water");
	
	List<Song> shouldBe = new ArrayList<Song>();
	shouldBe.add(eins);
	shouldBe.add(zwei);
	shouldBe.add(drei);
for (int i = 0; i < shouldBe.size(); i++) {
	assertEquals(found.get(i).getAlbum(),shouldBe.get(i).getAlbum());
	assertEquals(found.get(i).getArtist(),shouldBe.get(i).getArtist());
	assertEquals(found.get(i).getPath(),shouldBe.get(i).getPath());
	assertEquals(found.get(i).getTitle(),shouldBe.get(i).getTitle());
	
	}
}


@Test 
public void testShowAllPlaylists() throws Exception {
	List<Playlist> found = Database.showAllPlaylists();

	
	Playlist eins = new Playlist("Rock");
	
	List<Playlist> shouldBe = new ArrayList<>();
	shouldBe.add(eins);
	
	assertEquals(found.get(0).getPlaylistName(),shouldBe.get(0).getPlaylistName());
	assertEquals(found.get(0).getSize(),shouldBe.get(0).getSize());
	assertEquals(found.get(0).getSortStatus(),shouldBe.get(0).getSortStatus());
	
} 


@Test 
public void testremoveSongOfAllPlaylists() throws Exception {
	File expectedXmlFile = new File("expecteddatasetRemoveSongOfAllPlaylistsTest.xml");
	FlatXmlDataSetBuilder builder = new FlatXmlDataSetBuilder();
	IDataSet expectedDataSet = builder.build(expectedXmlFile);
	ITable expectedTable = expectedDataSet.getTable("Playlists");
	
	Database.removeSongOfAllPlaylists(Database.findSongByPath("1234.mp3").getSongId());
	
	IDatabaseConnection connection = getDatabaseConnection();
	IDataSet actualDataSet = connection.createDataSet();
	ITable actualTable = actualDataSet.getTable("Playlists");
	
	Assertion.assertEquals(expectedTable, actualTable);
}


@Test 
public void testDeletePlaylist() throws Exception {
	File expectedXmlFile = new File("expecteddatasetDeletePlaylistTest.xml");
	FlatXmlDataSetBuilder builder = new FlatXmlDataSetBuilder();
	IDataSet expectedDataSet = builder.build(expectedXmlFile);
	ITable expectedTable = expectedDataSet.getTable("Playlists");
	
	Database.deletePlaylist(1);
	
	IDatabaseConnection connection = getDatabaseConnection();
	IDataSet actualDataSet = connection.createDataSet();
	ITable actualTable = actualDataSet.getTable("Playlists");
	
	Assertion.assertEquals(expectedTable, actualTable);
}


@Test
public void testIsInDatabaseSongTrueTitle() {
	boolean result = Database.isInDatabaseSong("Waterfall", "title");
	assertEquals(result, true);
}


@Test
public void testIsInDatabaseSongTrueArtist() {
	boolean result = Database.isInDatabaseSong("Joe", "artist");
	assertEquals(result, true);
}


@Test
public void testIsInDatabaseSongTruePath() {
	boolean result = Database.isInDatabaseSong("1234.mp3", "path");
	assertEquals(result, true);
}


@Test
public void testIsInDatabaseSongTrueAlbum() {
	boolean result = Database.isInDatabaseSong("Remove", "album");
	assertEquals(result, true);
}


@Test
public void testIsInDatabaseSongFalseTitle() {
	boolean result = Database.isInDatabaseSong("41241", "title");
	assertEquals(result, false);
}


@Test
public void testIsInDatabaseSongFalseArtist() {
	boolean result = Database.isInDatabaseSong("31245", "artist");
	assertEquals(result, false);
}


@Test
public void testIsInDatabaseSongFalsePath() {
	boolean result = Database.isInDatabaseSong("Aua.mp3", "path");
	assertEquals(result, false);
}


@Test
public void testIsInDatabaseSongFalseAlbum() {
	boolean result = Database.isInDatabaseSong("312415555", "album");
	assertEquals(result, false);
}


@Test
public void testSortPlaylist() throws MalformedURLException, SQLException, ClassNotFoundException, DatabaseUnitException {
	File expectedXmlFile = new File("expecteddatasetSortPlaylist.xml");
	FlatXmlDataSetBuilder builder = new FlatXmlDataSetBuilder();
	IDataSet expectedDataSet = builder.build(expectedXmlFile);
	ITable expectedTable = expectedDataSet.getTable("Playlists");
	
	Database.sortPlaylist(1, 2);
	
	IDatabaseConnection connection = getDatabaseConnection();
	IDataSet actualDataSet = connection.createDataSet();
	ITable actualTable = actualDataSet.getTable("Playlists");
	
	Assertion.assertEquals(expectedTable, actualTable);
}


@Test
public void testSetSongTitle() throws MalformedURLException, SQLException, ClassNotFoundException, DatabaseUnitException, MP3PlayerException{
	File expectedXmlFile = new File("expecteddatasetSetSongTitle.xml");
	FlatXmlDataSetBuilder builder = new FlatXmlDataSetBuilder();
	IDataSet expectedDataSet = builder.build(expectedXmlFile);
	ITable expectedTable = expectedDataSet.getTable("Songs");
	
	Database.setSongTitle("AK-47", 2);
	
	IDatabaseConnection connection = getDatabaseConnection();
	IDataSet actualDataSet = connection.createDataSet();
	ITable actualTable = actualDataSet.getTable("Songs");
	
	Assertion.assertEquals(expectedTable, actualTable);
}


@Test
public void testSetSongArtist() throws MalformedURLException, SQLException, ClassNotFoundException, DatabaseUnitException, MP3PlayerException{
	File expectedXmlFile = new File("expecteddatasetSetSongArtist.xml");
	FlatXmlDataSetBuilder builder = new FlatXmlDataSetBuilder();
	IDataSet expectedDataSet = builder.build(expectedXmlFile);
	ITable expectedTable = expectedDataSet.getTable("Songs");
	
	Database.setSongArtist("Nelly", 2);
	
	IDatabaseConnection connection = getDatabaseConnection();
	IDataSet actualDataSet = connection.createDataSet();
	ITable actualTable = actualDataSet.getTable("Songs");
	
	Assertion.assertEquals(expectedTable, actualTable);
}


@Test
public void testSetSongAlbum() throws MalformedURLException, SQLException, ClassNotFoundException, DatabaseUnitException, MP3PlayerException{
	File expectedXmlFile = new File("expecteddatasetSetSongAlbum.xml");
	FlatXmlDataSetBuilder builder = new FlatXmlDataSetBuilder();
	IDataSet expectedDataSet = builder.build(expectedXmlFile);
	ITable expectedTable = expectedDataSet.getTable("Songs");
	
	Database.setSongAlbum("Sometimes", 2);
	
	IDatabaseConnection connection = getDatabaseConnection();
	IDataSet actualDataSet = connection.createDataSet();
	ITable actualTable = actualDataSet.getTable("Songs");
	
	Assertion.assertEquals(expectedTable, actualTable);
}


@Test
public void testAddLatestSongToPlaylist() throws MalformedURLException, SQLException, ClassNotFoundException, DatabaseUnitException, MP3PlayerException{
	File expectedXmlFile = new File("expecteddatasetAddLatestSongToPlaylist.xml");
	FlatXmlDataSetBuilder builder = new FlatXmlDataSetBuilder();
	IDataSet expectedDataSet = builder.build(expectedXmlFile);
	ITable expectedTable = expectedDataSet.getTable("Playlists");
	
	Database.addLatestSongToPlaylist();
	
	IDatabaseConnection connection = getDatabaseConnection();
	IDataSet actualDataSet = connection.createDataSet();
	ITable actualTable = actualDataSet.getTable("Playlists");
	
	Assertion.assertEquals(expectedTable, actualTable);
}

}

