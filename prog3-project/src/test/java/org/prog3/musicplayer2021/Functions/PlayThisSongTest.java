package org.prog3.musicplayer2021.Functions;

import static org.junit.jupiter.api.Assertions.*;
import java.io.IOException;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.prog3.musicplayer2021.Exceptions.PlaylistException;
import org.prog3.musicplayer2021.Exceptions.SongException;
import java.util.*;


/**
 * TestClass for PlayThisSong
 * @author paul kiel
 *
 */
class PlayThisSongTest {

	//Song: title, artist, path, album
	//Song: title, artist, path
	//Playlist: playlistName
	//Playlist: playlistName, offset

	private Playlist workingPlaylist;
	private Song workingSong;
	private Song workingSong2;
	private PlayThisSong playThatSong;
	private List<Song> tempList = new LinkedList<Song>();
	
	/**
	 * setUp initializes workingSong, workingPlaylist and playThatSong
	 * @throws SongException
	 * @throws IOException
	 * @throws PlaylistException
	 */
	@BeforeEach
	void setUp() throws SongException, IOException, PlaylistException {
		workingSong = new Song("WhyNot", "Unknown", " Unknown - Whynot.mp3", "why");
		workingSong2 = new Song("new", "Song", "Song - new.mp3");
		workingPlaylist = new Playlist("thisIsAWorkingPlaylist");
		playThatSong = new PlayThisSong();
	}

	/**
	 * currentPlaylist workingPlaylist should be null because there are no songs in workingPlaylist
	 */
	@Test
	void testSetCurrentPlaylistIsNull() {
		assertThrows(PlaylistException.class, () -> {
			workingPlaylist.addSongToPlaylist(null);
		});
	}
	
	/**
	 * currentPlaylist should be correct because the songs in playlist are correct as well
	 * @throws PlaylistException
	 * @throws SongException
	 * @throws IOException
	 */
	@Test
	void testSetCurrentPlaylistIsCorrect() throws PlaylistException, SongException, IOException{
		tempList.add(workingSong2);
		tempList.add(workingSong);
		workingPlaylist.addSongToPlaylist(workingSong);
		workingPlaylist.addSongToPlaylist(workingSong2);
		playThatSong.setCurrentPlaylist(workingPlaylist);
		assertEquals(workingPlaylist.getSonglist(), tempList);
	}
	
	
	/**
	 * Playlist workingPlaylist should not contain Song workingSong
	 * @throws PlaylistException 
	 */
	@Test
	void testSetCurrentSongDoesNotContainSong() {
		assertThrows(SongException.class, () -> {	
			workingPlaylist.addSongToPlaylist(workingSong);
				playThatSong.setCurrentPlaylist(workingPlaylist);
				playThatSong.setCurrentSong(workingSong2);
		});
	}
	
	/**
	 * the currentSong should be null
	 */
	@Test
	void testSetCurrentSongIsNull() {
		assertThrows(SongException.class, () -> {
			workingPlaylist.addSongToPlaylist(workingSong);
			playThatSong.setCurrentPlaylist(workingPlaylist);
			playThatSong.setCurrentSong(null);
		});
	}
	
	/**
	 * the currentSong workingSong should be correct
	 * @throws SongException
	 * @throws PlaylistException
	 * @throws IOException
	 */
	@Test
	void testSetCurrentSongIsCorrect() throws PlaylistException, IOException, SongException {
		workingPlaylist.addSongToPlaylist(workingSong);
		playThatSong.setCurrentPlaylist(workingPlaylist);
		playThatSong.setCurrentSong(workingSong);
		assertEquals(playThatSong.getCurrentSong(), workingSong);
	}
	
	/**
	 * the newNextSong should be null because the currentSong is null
	 */
	@Test
	void testGetNewNextSongPlaylistIteratorIsNull() {
		assertThrows(SongException.class, () -> {
			//workingPlaylist.addSongToPlaylist(null);
			playThatSong.setCurrentPlaylist(workingPlaylist);
			playThatSong.setCurrentSong(null);
			playThatSong.getNewNextSong().equals(null);
		});
	}

	/**
	 * getNewNextSong needs to be called twice 
	 * the newNextSong should be workingSong while workingPlaylist has only one song. 
	 * The currentSong is set to workingSong
	 * the newNextSong should be workingSong2 while workingPlaylist has two songs. 
	 * The currentSong is set to workingSong
	 * @throws PlaylistException 
	 * @throws IOException 
	 * @throws SongException 
	 */
	@Test
	void testGetNewNextSongPlaylistIteratorIsCorrect() throws PlaylistException, IOException, SongException {
		workingPlaylist.addSongToPlaylist(workingSong);
		playThatSong.setCurrentPlaylist(workingPlaylist);
		playThatSong.setCurrentSong(workingSong);
		playThatSong.getNewNextSong();
		assertEquals(playThatSong.getNewNextSong(),workingSong);
		//to check if a playlist with more than one songs is working 
		workingPlaylist.addSongToPlaylist(workingSong2);
		playThatSong.setCurrentPlaylist(workingPlaylist);
		playThatSong.setCurrentSong(workingSong);
		playThatSong.getNewNextSong();
		assertEquals(playThatSong.getNewNextSong(),workingSong2);
	}
	
	/**
	 * currentPlaylist workingPlaylist should be null because workingPlaylist has no songs
	 */
	@Test
	void testGetNewNextSongCurrentPlaylistIsNull() {
		assertThrows(PlaylistException.class, () -> {
			workingPlaylist.addSongToPlaylist(null);
		});
	}
	
	/**
	 * currentPlaylist workingPlaylist should be correct because the songs in workingPlaylist are correct as well
	 * @throws SongException
	 * @throws PlaylistException 
	 * @throws IOException 
	 */
	@Test
	void testGetNewNextSongCurrentPlaylistIsCorrect() throws PlaylistException, IOException, SongException {
		tempList.add(workingSong2);
		tempList.add(workingSong);		
		workingPlaylist.addSongToPlaylist(workingSong);
		workingPlaylist.addSongToPlaylist(workingSong2);
		playThatSong.setCurrentPlaylist(workingPlaylist);
		playThatSong.setCurrentSong(workingSong);
		assertEquals(workingPlaylist.getSonglist(), tempList);
	}
	
	/**
	 * the newPreviousSong should be null while workingPlaylist has only one song. 
	 * The currentSong is set to null
	 */
	@Test
	void testGetNewPreviousSongPlaylistIteratorIsNull() {
		assertThrows(SongException.class, () -> {
			workingPlaylist.addSongToPlaylist(workingSong);
			playThatSong.setCurrentPlaylist(workingPlaylist);
			playThatSong.setCurrentSong(null);
		});
	}
	
	/**
	 * 
	 * the newPreviousSong should be workingSong while workingPlaylist has only one song. 
	 * The currentSong is set to workingSong
	 * the newPreviousSong should be workingSong2 while workingPlaylist has two songs. 
	 * The currentSong is set to workingSong
	 * @throws PlaylistException 
	 * @throws IOException 
	 * @throws SongException 
	 */
	@Test
	void testGetNewPreviousSongPlaylistIteratorIsCorrect() throws SongException, IOException, PlaylistException {
		workingPlaylist.addSongToPlaylist(workingSong);
		playThatSong.setCurrentPlaylist(workingPlaylist);
		playThatSong.setCurrentSong(workingSong);
		assertEquals(playThatSong.getNewPreviousSong(),workingSong);
		//to check if a playlist with more than one songs is working 
		workingPlaylist.addSongToPlaylist(workingSong2);
		playThatSong.setCurrentPlaylist(workingPlaylist);
		playThatSong.setCurrentSong(workingSong);
		assertEquals(playThatSong.getNewPreviousSong(),workingSong2);
		
	}
	
	/**
	 * the currentSong should be null
	 */
	@Test
	void testGetNewPreviousSongCurrentSongIsNull() {
		assertThrows(PlaylistException.class, () -> {
			workingPlaylist.addSongToPlaylist(null);
		});
	}

	/**
	 * the currentSong should be workingSong 
	 * @throws SongException
	 * @throws IOException
	 * @throws PlaylistException
	 */
	@Test
	void testGetNewPreviousSongCurrentSongIsCorrecct() throws SongException, IOException, PlaylistException {
		workingPlaylist.addSongToPlaylist(workingSong);
		playThatSong.setCurrentPlaylist(workingPlaylist);
		playThatSong.setCurrentSong(workingSong);
		assertEquals(playThatSong.getCurrentSong(), workingSong);
	}
	
	/**
	 * currentPlaylist workingPlaylist should be null because the only song in currentPlaylist is null
	 */
	@Test
	void testRamdomizePlaylistCurrentPlaylistIsNull() {
		assertThrows(PlaylistException.class, () -> {
			workingPlaylist.addSongToPlaylist(null);
		});
	}
	
	/**
	 * the currentPlaylist workingPlaylist should be correct because the songs in workingPlaylist are correct
	 * @throws PlaylistException
	 * @throws SongException
	 * @throws IOException
	 */
	@Test
	void testRandomizePlaylistCurrentPlaylistIsCorrect() throws PlaylistException, SongException, IOException {
		tempList.add(workingSong2);
		tempList.add(workingSong);
		workingPlaylist.addSongToPlaylist(workingSong);
		workingPlaylist.addSongToPlaylist(workingSong2);
		playThatSong.setCurrentPlaylist(workingPlaylist);
		assertEquals(workingPlaylist.getSonglist(), tempList);
	}
	
	/**
	 * the currentPlaylist workingPlayist should be null because currentPlaylist has no songs
	 */
	@Test
	void testUpdateNextSongCurrentPlaylistIsNull() {
		assertThrows(PlaylistException.class, () -> {
			workingPlaylist.addSongToPlaylist(null);
		});
	}

	/**
	 * the newNextSong should be currentSong because currentPlaylist is one 
	 * @throws PlaylistException 
	 * @throws IOException 
	 * @throws SongException 
	 */
	@Test
	void testUpdateNextSongCurrentPlaylistIsOne() throws SongException, IOException, PlaylistException {
		tempList.add(workingSong);
		workingPlaylist.addSongToPlaylist(workingSong);
		playThatSong.setCurrentPlaylist(workingPlaylist);
		playThatSong.setCurrentSong(workingSong);
		playThatSong.getNewNextSong();
		assertEquals(playThatSong.getNewNextSong(), workingSong);
		assertEquals(workingPlaylist.getSonglist(), tempList);
	}
	
	/**
	 * the currentPlaylist should be correct because the songs in workingPlaylsit are correct
	 * @throws PlaylistException
	 * @throws SongException
	 * @throws IOException
	 */
	@Test
	void testUpdateNextSongCurrentPlaylistIsCorrect() throws PlaylistException, SongException, IOException {
		tempList.add(workingSong2);
		tempList.add(workingSong);
		workingPlaylist.addSongToPlaylist(workingSong);
		workingPlaylist.addSongToPlaylist(workingSong2);
		workingPlaylist.getSonglist();
		playThatSong.setCurrentPlaylist(workingPlaylist);
		playThatSong.setCurrentSong(workingSong);
		assertEquals(workingPlaylist.getSonglist(), tempList);
		assertEquals(playThatSong.getCurrentSong(), workingSong);
		//nextSong = workingSong2
		playThatSong.getNewNextSong();
		assertEquals(playThatSong.getNewNextSong(), workingSong2);

		
	}
	
	/**
	 * currentPlaylist workingPlaylist should be null because workinPlaylist has no songs
	 */
	@Test
	void testUpdatePreviousSongCurrentPlaylistIsNull() {
		assertThrows(PlaylistException.class, () -> {
			workingPlaylist.addSongToPlaylist(null);
		});
	}

	/**
	 * the currentPlaylist should be correct because the only song in currentPlaylist is correct
	 * @throws SongException
	 * @throws IOException
	 * @throws PlaylistException
	 */
	@Test
	void testUpdatePreviousSongCurrentPlaylistIsOne() throws SongException, IOException, PlaylistException {
		tempList.add(workingSong);
		workingPlaylist.addSongToPlaylist(workingSong);
		playThatSong.setCurrentPlaylist(workingPlaylist);
		playThatSong.setCurrentSong(workingSong);
		assertEquals(playThatSong.getNewPreviousSong(), workingSong);
		assertEquals(workingPlaylist.getSonglist(), tempList);
	}
	
	//remove next
	/**
	 * the currentPlaylist should be correct because the songs in currentPlaylist are correct
	 * @throws PlaylistException
	 * @throws SongException
	 * @throws IOException
	 */
	@Test
	void testUpdatePreviousSongCurrentPlaylistIsCorrect() throws PlaylistException, SongException, IOException{
		tempList.add(workingSong2);
		tempList.add(workingSong);
		workingPlaylist.addSongToPlaylist(workingSong);
		workingPlaylist.addSongToPlaylist(workingSong2);
		workingPlaylist.getSonglist();
		playThatSong.setCurrentPlaylist(workingPlaylist);
		playThatSong.setCurrentSong(workingSong);
		assertEquals(workingPlaylist.getSonglist(), tempList);
		assertEquals(playThatSong.getCurrentSong(), workingSong);
		//previousSong = workingSong2
		assertEquals(playThatSong.getNewPreviousSong(), workingSong2);
	}
}

