package org.prog3.musicplayer2021.Functions;

import org.prog3.musicplayer2021.Exceptions.*;
import static org.junit.jupiter.api.Assertions.*;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 * TestClass for Playlist
 * @author pascal ney
 *
 */
class PlaylistTest{
	private Song testSongQueenUnderPressure;
	private Song testSongNFWait;
	private Song testSong1;
	private Playlist testPlaylistMain;
	private Playlist testPlaylistUnsorted;
	private List<Song> sortedByArtist;
	
	@BeforeEach
	void setUp() throws PlaylistException, SongException, IOException{
		
		testPlaylistMain = new Playlist("testPlaylistMain");
		testSongQueenUnderPressure = new Song("Under Pressure", "Queen", "Under Pressure - Queen.mp3");
		testSongNFWait = new Song("Wait", "NF", "Wait - NF.mp3");
		testSong1 = new Song("Test", "Song", "Song - Test.mp3");
		testPlaylistMain.addSongToPlaylist(testSongNFWait);
		testPlaylistMain.addSongToPlaylist(testSongQueenUnderPressure);
		testPlaylistMain.addSongToPlaylist(testSong1);
		testPlaylistUnsorted = new Playlist("testPlaylistUnsorted", 12);
		testPlaylistUnsorted.addSongToPlaylist(testSongNFWait);
		testPlaylistUnsorted.addSongToPlaylist(testSongQueenUnderPressure);
		testPlaylistUnsorted.addSongToPlaylist(testSong1);
		sortedByArtist = new LinkedList<Song>();
		sortedByArtist.add(testSongNFWait);
		sortedByArtist.add(testSongQueenUnderPressure);
		sortedByArtist.add(testSong1);
		
	}
	
	
	

	@Test  
	public void testPlaylistEmptyName() {
		assertThrows(PlaylistException.class, () -> {
			new Playlist("");
		});
	}
	
	@Test 
	public void testPlaylistNullName() {
		assertThrows(PlaylistException.class, () -> {
			new Playlist(null);
		});

    }
	
	@Test
	public void testPlaylistCorrectConstructor() throws PlaylistException {
		Playlist playlist = new Playlist("test");
		assertEquals(playlist.getPlaylistName(), "test");
	}
	
	@Test
	public void testPlaylistNameCorrect() throws PlaylistException{
		Playlist testNamePlaylist = new Playlist("TestName");
		assertEquals(testNamePlaylist.getPlaylistName(), "TestName" );
	}
	
	@Test
	public void testPlaylistAdd() throws PlaylistException {
		Playlist testPlaylist = new Playlist("testPlaylist");
		testPlaylist.addSongToPlaylist(testSongNFWait);
		assertEquals(testPlaylist.getSize(), 1);
		testPlaylist.addSongToPlaylist(testSongQueenUnderPressure);
		assertEquals(testPlaylist.getSize(), 2);
	}
	
	@Test
	public void testPlaylistAddFailNull() throws PlaylistException {
		Playlist testPlaylist = new Playlist("testName");
		assertThrows(PlaylistException.class, () -> {
			testPlaylist.addSongToPlaylist(null);
		});
		
	}
	
	@Test
	public void testPlaylistGetSongList() throws PlaylistException {
		assertEquals(testPlaylistMain.getSize(), 
				testPlaylistMain.getSonglist().size());
		for(int i = 0; i < testPlaylistMain.getSize(); i++) {
			assertEquals(testPlaylistMain.getSongByIndex(i), 
					testPlaylistMain.getSonglist().get(i));
		}
	}
	
	@Test
	public void testPlaylistSortByTitle() throws PlaylistException {
		List<Song> testList = new LinkedList<Song>();
		testList.add(testSongNFWait);
		testList.add(testSongQueenUnderPressure);
		
		testList.add(testSong1);
		for(int i = 0; i < testList.size(); i++) {
			assertEquals(testPlaylistUnsorted.getSongByIndex(i), testList.get(i));
		}
		List<Song> testListSorted = new LinkedList<Song>();
		testList.add(testSong1);
		testList.add(testSongQueenUnderPressure);
		testList.add(testSongNFWait);
		testPlaylistMain.sortByTitle();
		for(int i = 0; i < testListSorted.size(); i++) {
			assertEquals(testPlaylistUnsorted.getSongByIndex(i), testList.get(i));
		}
			
		
	}
	//title, artist, path, album
	@Test
	public void testPlaylistAddSongSuccess() throws SongException, IOException, PlaylistException {
		Song testSong = new Song("a", "b", "b - a.mp3", "d");
		testPlaylistMain.addSongToPlaylist(testSong);
		assertTrue(testPlaylistMain.getSonglist().contains(testSong));
	}
	
	@Test
	public void testPlaylistRemoveSong() throws PlaylistException {
		assertTrue(testPlaylistMain.getSonglist().
				contains(testSongQueenUnderPressure));
		testPlaylistMain.removeSongByPath("Under Pressure - Queen.mp3");
		assertFalse(testPlaylistMain.getSonglist().contains(testSongQueenUnderPressure));
	}
	
	@Test
	public void testPlaylistRemoveSongNull() {
		assertThrows(PlaylistException.class, () -> {
			testPlaylistMain.removeSongByPath(null);
		});
	}
	
	@Test
	public void testPlaylistRemoveSongEmpty() {
		assertThrows(PlaylistException.class, () -> {
			testPlaylistMain.removeSongByPath("");
		});
	}
	
	@Test
	public void testPlaylistRemoveSongNotThere() {
		assertThrows(PlaylistException.class, () -> {
			testPlaylistMain.removeSongByPath("yeee");
		});
	}
	
	@Test
	public void testPlaylistFindSongByPath() throws PlaylistException {
		assertEquals(testPlaylistMain.findSongByPath("Wait - NF.mp3"), testSongNFWait);
	}
	
	@Test
	public void testPlaylistFindSongByPathNull() {
		assertThrows(PlaylistException.class, () -> {
			testPlaylistMain.findSongByPath(null);
		});
	}
	
	@Test
	public void testPlaylistFindSongByPathEmpty() {
		assertThrows(PlaylistException.class, () -> {
			testPlaylistMain.findSongByPath("");
		});
	}
	
	@Test
	public void testPlaylistFindSongByPathNotThere(){
		assertThrows(PlaylistException.class, () -> {
			testPlaylistMain.findSongByPath("eeee");
		});
	}
	
	@Test
	public void testPlaylistUpdateSorting() throws PlaylistException {
		//Sort by artist
		testPlaylistMain.updateSorting(2);
		for(int i = 0; i < testPlaylistMain.getSize(); i++) {
			assertEquals(testPlaylistMain.getSongByIndex(i), sortedByArtist.get(i));
		}
		
	}
	
}
