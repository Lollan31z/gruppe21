package org.prog3.musicplayer2021.Functions;

import static org.junit.jupiter.api.Assertions.*;
import java.io.IOException;
import org.junit.jupiter.api.Test;
import org.prog3.musicplayer2021.Exceptions.SongException;

/**
 * TestClass for Song
 * @author paul kiel
 *
 */
class SongTest {
	//String title, String artist, String path, String album

	/**
	 * Should return true everywhere
	 */
	private Song trueSong;
	private Song anotherTrueSong;

	/**
	 * setUp initializes trueSong
	 * @throws SongException
	 * @throws IOException
	 */
	/*@BeforeEach
	void setUp() throws SongException, IOException {
		trueSong = new Song("Wahrscheinlichkeitsrechnung Teil 1", "Peter Birkner", 
				"Peter Birkner - Wahrscheinlichkeitsrechnung Teil 1.mp3", "Wahrscheinlichkeitsrechung");
		anotherTrueSong = new Song("We Will Rock You", "Queen",
				"Queen - We Will Rock You.mp3");
	}*/
	
	/**
	 * should throw a SongException because the path does not end with .mp3
	 * should work if the ending is missing completely or just ends with mp3
	 */
	@Test
	void testConstructorPathWithoutMp3() {
		assertThrows(SongException.class, () -> {
			 new Song("Wahrscheinlichkeitsrechnung Teil 1", "Peter Birkner", 
					"Peter Birkner - Wahrscheinlichkeitsrechnung Teil 1", "Wahrscheinlichkeitsrechung");
		});
	}

	/**
	 * should throw a SongException because the path is empty
	 */
	@Test
	void testConstructorPathIsEmpty() {
		assertThrows(SongException.class, () -> {
			new Song("Wahrscheinlichkeitsrechnung Teil 1", "Peter Birkner", 
					"", "Wahrscheinlichkeitsrechnung");
		});
	}
	/**
	 * should throw a SongException because the path is null
	 */
	@Test
	void testConstructorPathIsNull() {
		assertThrows(SongException.class, () -> {
			new Song("Wahrscheinlichkeitsrechnung Teil 1", "Peter Birkner", 
					null, "Wahrscheinlichkeitsrechung");
		});
	}
	/**
	 * should not throw an Exception because the path should be named correctly
	 * @throws SongException
	 * @throws IOException
	 */
	@Test
	void testConstructorPathIsCorrect() throws SongException, IOException {
		trueSong = new Song("Wahrscheinlichkeitsrechnung Teil 1", "Peter Birkner", 
				"Peter Birkner - Wahrscheinlichkeitsrechnung Teil 1.mp3", "Wahrscheinlichkeitsrechung");
		anotherTrueSong = new Song("We Will Rock You", "Queen",
				"Queen - We Will Rock You.mp3");
		assertEquals(trueSong.getPath(), "Peter Birkner - Wahrscheinlichkeitsrechnung Teil 1.mp3");
		assertEquals(anotherTrueSong.getPath(), "Queen - We Will Rock You.mp3");
	}
	/**
	 * should throw a SongException because artist is empty
	 */
	@Test
	void testConstructorArtistIsEmpty() {
		assertThrows(SongException.class, () -> {
			new Song("Wahrscheinlichkeitsrechnung Teil 1", "", 
					"Peter Birkner - Wahrscheinlichkeitsrechnung Teil 1.mp3", "Wahrscheinlichkeitsrechung");
		});
	}
	/**
	 * should throw a SongException because artist is null
	 */
	@Test
	void testConstructorArtistIsNull() {
		assertThrows(SongException.class, () -> {
			new Song("Wahrscheinlichkeitsrechnung Teil 1", null, 
				"Peter Birkner - Wahrscheinlichkeitsrechnung Teil 1.mp3", "Wahrscheinlichkeitsrechung");
		});
	}
	/**
	 * should not return an Exception because the artist should be named correctly
	 * @throws SongException
	 * @throws IOException
	 */
	@Test
	void testConstructorArtistIsCorrect() throws SongException, IOException {
		trueSong = new Song("Wahrscheinlichkeitsrechnung Teil 1", "Peter Birkner", 
				"Peter Birkner - Wahrscheinlichkeitsrechnung Teil 1.mp3", "Wahrscheinlichkeitsrechung");
		anotherTrueSong = new Song("We Will Rock You", "Queen",
				"Queen - We Will Rock You.mp3");
		assertEquals(trueSong.getArtist(), "Peter Birkner");
		assertEquals(anotherTrueSong.getArtist(), "Queen");
	}	
	/**
	 * should throw a SongException because title is empty
	 */
	@Test
	void testConstructorTitleIsEmpty() {
		assertThrows(SongException.class, () -> {
			new Song("", "Peter Birkner", 
				"Peter Birkner - Wahrscheinlichkeitsrechnung Teil 1.mp3", "Wahrscheinlichkeitsrechung");
		});
	}
	/**
	 * should throw a SongException because title is null
	 */
	@Test
	void testConstructorTitleIsNull() {
		assertThrows(SongException.class, () -> {
			new Song(null, "Peter Birkner", 
				"Peter Birkner - Wahrscheinlichkeitsrechnung Teil 1.mp3", "Wahrscheinlichkeitsrechung");
		});
	}
	/**
	 * should not return an Exception because the title should be named correctly
	 * @throws SongException
	 * @throws IOException
	 */
	@Test
	void testConstructorTitleTrue() throws SongException, IOException {
		trueSong = new Song("Wahrscheinlichkeitsrechnung Teil 1", "Peter Birkner", 
				"Peter Birkner - Wahrscheinlichkeitsrechnung Teil 1.mp3", "Wahrscheinlichkeitsrechung");
		anotherTrueSong = new Song("We Will Rock You", "Queen",
				"Queen - We Will Rock You.mp3");
		assertEquals(trueSong.getTitle(), "Wahrscheinlichkeitsrechnung Teil 1");
		assertEquals(anotherTrueSong.getTitle(), "We Will Rock You");
		
	}
}
