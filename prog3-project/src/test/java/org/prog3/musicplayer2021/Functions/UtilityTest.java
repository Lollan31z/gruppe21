package org.prog3.musicplayer2021.Functions;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
import org.prog3.musicplayer2021.Exceptions.UtilityException;
/**
 * TestClass for Utility
 * @author pascal ney
 *
 */
class UtilityTest {

	@Test
	void testPathEndsWithMP3() throws UtilityException {
		assertTrue(Utility.pathEndsWithMP3("yee.mp3"));
		assertFalse(Utility.pathEndsWithMP3("argh"));
		assertFalse(Utility.pathEndsWithMP3("erghmp3"));
		assertFalse(Utility.pathEndsWithMP3(".mp3eehde"));
		assertFalse(Utility.pathEndsWithMP3(""));
	}
	
	@Test
	void testPathEndsWithMP3FailNull() {
		assertThrows(UtilityException.class, () -> {
			Utility.pathEndsWithMP3(null);
		});
	}
	
	@Test
	void testGetArtistByPath() throws UtilityException {
		assertEquals(Utility.getArtistByPath("Nice Music - WOW.mp3"), "WOW");
		assertEquals(Utility.getArtistByPath("blablabla.mp3"), "Various Artists");
		assertEquals(Utility.getArtistByPath("-.mp3"), "Various Artists");
	}
	
	void testGetArtistByPathFailEndsWithMP3() {
		assertThrows(UtilityException.class, () -> {
			Utility.getArtistByPath("alabe");
		});
	}
	
	@Test
	void testGetArtistByPathFailNull() {
		assertThrows(UtilityException.class, () -> {
			Utility.getArtistByPath(null);
		});
	}
	
	@Test
	void testGetTitleByPath() throws UtilityException {
		assertEquals(Utility.getTitleByPath("Nice Music - WOW.mp3"), "Nice Music");
	}
	
	@Test
	void testGetTitleByPathWrongStructure() throws UtilityException {
		assertEquals(Utility.getTitleByPath("blablabla.mp3"), "blablabla");
	}
	
	@Test
	void testGetTitleByPathEmptyTitleField() throws UtilityException {
		assertEquals(Utility.getTitleByPath("- arabbvjhb.mp3"), "Untitled");
	}
	
	@Test
	void testGetTitleByPathFailNull() {
		assertThrows(UtilityException.class, () -> {
			Utility.getTitleByPath(null);
		});
	}
	
	@Test
	void testGetTitleByPathFailEndsWithMP3() {
		assertThrows(UtilityException.class, () -> {
			Utility.getTitleByPath("eiuzhgefiuwk");
		});
	}
	
	@Test
	void testGetRelativePathOnlyBackSlashes() throws UtilityException {
		assertEquals(Utility.getRelativePath("abbe\\ebbe\\labba"), "labba");			
	}
	
	@Test
	void testGetRelativePathOnlyNormalSlashes() throws UtilityException {
		assertEquals(Utility.getRelativePath("yee/ghe/eukzgfuiwk"), "eukzgfuiwk");
	}
	
	@Test
	void testGetRelativePathBothSlashesEndWithNormalSlash() throws UtilityException {
		assertEquals(Utility.getRelativePath("eeee/alla\\abbe/ree"), "ree");
	}
	
	@Test
	void testGetRelativePathBothSlashesEndWithBackSlash() throws UtilityException {
		assertEquals(Utility.getRelativePath("eukzgfuiwk\\terq/ree\\aaa"), "aaa");
	}
	
	
	
	@Test
	void testGetRelativePathFailEmptyEndWithNormalSlash() {
		assertThrows(UtilityException.class, () -> {
			Utility.getRelativePath("aa/reget/");
		});
	}
	
	@Test
	void testGetRelativePathFailEmptyEndWithBackSlash() {
		assertThrows(UtilityException.class, () -> {
			Utility.getRelativePath("aa/reget\\");
		});
	}
	
	@Test
	void testGetRelativePathFailEmptyPath() {
		assertThrows(UtilityException.class, () -> {
			Utility.getRelativePath("");
		});
	}
	
	@Test
	void testGetRelativePathFailNoSlashes() {
		assertThrows(UtilityException.class, () -> {
			Utility.getRelativePath("arrrr");			
		});
	}
	
	@Test
	void testGetRelativePathFailNull() {
		assertThrows(UtilityException.class, () -> {
			Utility.getRelativePath(null);
		});
	}
	

}
